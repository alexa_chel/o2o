<?php namespace ProcessWire; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/parallax.min.js"></script>
    <title>Offline-to-Online</title>
</head>
<style>
    html, body {
        width: 100%;
    }

    body {
        height: 100vh;
        margin: 0;
    }

    #bg {
        position: fixed;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        margin: auto;
        width: 30%;
        height: 40vh;
        min-width: 500px;
        z-index: -1;
    }

    .logo {
        width: 100%;
        height: 100%;
        background: url(<?= $config->urls->templates ?>img/logo-stub.png) center / cover no-repeat;
    }

    .hint {
        position: fixed;
        bottom: 41vh;
        left: 0;
        right: 0;
        margin: 0;
        font-family: "TT Norms", sans-serif;
        font-size: 12px;
        color: #bfbfbf;
        text-align: center;
    }
    
    @media (max-width: 640px) {
        .hint {
            bottom: 39vh;
        }
    }
</style>
<body>
<div id="bg">
    <img src="<?= $config->urls->templates ?>img/bg-stub.jpg" data-depth="0.6" alt="">
</div>
<div class="logo">
    <p class="hint">К сожалению в данный момент сайт не доступен.<br>
        Проводятся профилактические работы.</p>
</div>
</body>
<script>
    $(function () {
        new Parallax(document.getElementById('bg'));
    })
</script>
</html>