<?php namespace ProcessWire; ?>
<div class="page page-map">
    <a id="scroll-top">
        <img src="<?= $config->urls->templates ?>img/arrow-scroll-top.png" alt="">
    </a>

    <div class="section-first" style="padding-bottom: 0; height: initial; max-height: initial; max-width: initial; flex-direction: column">
        <div id="scene_1">
            <div class="first" data-depth="0.2">Карта</div>
            <div class="second" data-depth="0.6">покрытия</div>
        </div>

        <div class="communicate" id="map" style="max-width: initial; padding: 60px 0 0 0; position: relative">
            <div class="container" style="margin-bottom: 0">
                <div class="text-wrapper">
                    <p class="text1 strikethrough-wrapper3 wow fadeInLeft">
                        Мы <span class="text-wrapper">уже собрали<span class="wow strikethrough"></span></span> для вас данные о
                    </p>
                    <div class="unique-macs" id="unique-macs" data-from="1" style="font-size: 100px">
                        <img class="loader" src="<?= $config->urls->templates ?>img/loader.svg" alt="">
                    </div>
                    <p class="text2 strikethrough-wrapper3 wow fadeInRight">
                        <span>людях</span>, видевших рекламу на outdoor-конструкциях<br>в г. <label class="city"></label>
                    </p>
                </div>
            </div>
        </div>

        <div class="section-city-map map" data-city="<?= $page->cityName ?>">
            <div class="row" style="max-width: 1220px">
                <div class="owl-carousel-months owl-theme months" id="months" style="display: none">
                    <div id="month_1" data-number="1">Январь</div>
                    <div id="month_2" data-number="2">Февраль</div>
                    <div id="month_3" data-number="3">Март</div>
                    <div id="month_4" data-number="4">Апрель</div>
                    <div id="month_5" data-number="5">Май</div>
                    <div id="month_6" data-number="6">Июнь</div>
                    <div id="month_7" data-number="7">Июль</div>
                    <div id="month_8" data-number="8">Август</div>
                    <div id="month_9" data-number="9">Сентябрь</div>
                    <div id="month_10" data-number="10">Октябрь</div>
                    <div id="month_11" data-number="11">Ноябрь</div>
                    <div id="month_12" data-number="12">Декабрь</div>
                </div>
            </div>

            <div class="map-wrapper loader-wrapper">
                <div id="city-map"></div>
                <img class="loader" src="<?= $config->urls->templates ?>img/loader.svg" alt="">
            </div>
        </div>
    </div>
</div>