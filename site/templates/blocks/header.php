<?php namespace ProcessWire;
//if (!$user->isLoggedin()) {
//    $session->redirect($pages->get("template=stub")->url);
//}
?>

<?php if ($page->url != 'calculator') { ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width" id="viewport">
        <title>O2O media — cбор аудиторий рекламных конструкций и получение дохода от накопленных данных</title>
        <meta name="description" content="Федеральная компания, помогающая владельцам рекламных конструкций и торговых помещений по всей России подтвердить маркетинговые показатели об аудитории и ретаргетировать их в online">
        <meta name="Keywords" content="mac address spoofing, mac spoofing, o2o media, O2O, offline 2 online, offline to online, online to offline, wi fi аналитика, wi fi сенсор, wi fi сканер для сбора mac адресов, wifi радар для сбора mac адресов, наружная реклама russ outdoor, наружная реклама аутдор, реклама russ outdoor, рус аутдор наружная реклама, сбор mac адресов, сбор mac адресов wifi, сбор mac адресов wifi купить, сбор mac адресов для рекламы, Сниффер купить, Сниффер установить, Mac адрес">
        <meta name="yandex-verification" content="fb77648944cf4e84" />
        <meta http-equiv="Cache-Control" content="no-cache" />
        <meta property="og:title" content="O2O media — cбор аудиторий рекламных конструкций и получение дохода от накопленных данных" />
        <meta property="og:description" content="Федеральная компания, помогающая владельцам рекламных конструкций и торговых помещений по всей России подтвердить маркетинговые показатели об аудитории и ретаргетировать их в online" />
        <meta property="og:image" content="https://o2o.media/site/templates/img/media.png" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://o2o.media/" />
        <meta property="fb:app_id" content="612853856119216" />
        <link rel="shortcut icon" href="<?= $config->urls->templates ?>img/o2o_favicon.png" type="image/png">
        <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/jquery-spincrement.js"></script>
        <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/wow.min.js"></script>
        <script src="https://api-maps.yandex.ru/2.1/?apikey=85fde534-aeff-44b9-86b9-8db26efca963&lang=ru_RU"
                type="text/javascript"></script>
        <script src="<?= $config->urls->templates ?>scripts/jquery.viewportchecker.js" type="text/javascript"></script>
        <script src="<?= $config->urls->templates ?>scripts/jquery.maskedinput.min.js" type="text/javascript"></script>
        <script src="<?= $config->urls->templates ?>scripts/owl.carousel.min.js" type="text/javascript"></script>
        <script src="<?= $config->urls->templates ?>scripts/moment.min.js" type="text/javascript"></script>
        <script src="<?= $config->urls->templates ?>scripts/lazysizes.min.js" async=""></script>
        <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/parallax.min.js"></script>
        <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/jquery.fancybox-1.3.4.js"></script>
        <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/index.37.min.js" async></script>
        <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/fonts.css">
        <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/animate.css">
        <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/owl.carousel.min.css">
        <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/jquery.fancybox-1.3.4.css">
        <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/style.5.css">
        <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/cases.2.css">
        <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/documents.2.css">
        <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/device.2.css">
        <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/contacts.2.css">
        <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/reviews.6.css">
        <script src="//code.jivosite.com/widget.js" data-jv-id="nVf5AT60uk" async></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-157960803-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-157960803-1');
        </script>
    </head>
<body>
<header class="header">
    <div class="container row">
        <a class="header-logo" href="/"><img src="<?= $config->urls->templates ?>img/logo.png" alt="Логотип O2O.media"></a>
        <ul class="header-menu">
            <li class="header-menu__item"><a href="/device">Заказ оборудования</a></li>
            <li class="header-menu__item"><a href="<?php if ($page->url != '/') echo '/'?>#map">Карта покрытия</a></li>
            <li class="header-menu__item"><a href="<?php if ($page->url != '/') echo '/'?>#cases">Кейсы</a></li>
            <li class="header-menu__item"><a href="/reviews">Отзывы</a></li>
            <li class="header-menu__item"><a href="/contacts">Контакты</a></li>
        </ul>
        <div class="header-phone">
            <a class="tel:+78002503452">8-800-250-34-52</a>
        </div>
        <a class="btn-transparent" href="https://lk.o2o.media" target="_blank">Личный кабинет</a>
        <button type="button" class="burger">
            <div></div>
            <div></div>
            <div></div>
        </button>
        <ul class="header-menu header-mobile-menu">
            <li class="header-menu__item"><a href="/device">Заказ оборудования</a></li>
            <li class="header-menu__item"><a href="<?php if ($page->url != '/') echo '/'?>#map">Карта покрытия</a></li>
            <li class="header-menu__item"><a href="<?php if ($page->url != '/') echo '/'?>#cases">Кейсы</a></li>
            <li class="header-menu__item"><a href="/reviews">Отзывы</a></li>
            <li class="header-menu__item"><a href="/contacts">Контакты</a></li>
            <li class="header-menu__item"><a href="/documents">Документация</a></li>
            <li class="header-menu__item"><a href="<?= $config->urls->templates ?>privacy.pdf" target="_blank">Политика конфиденциальности</a></li>
            <li class="header-menu__item contacts">
                <a href="tel:+78002503452" class="contacts__phone">8-800-250-34-52</a>
                <a href="mailto:info@o2o.media" class="contacts__email">info@o2o.media</a>
                <div class="contacts__socials">
                    <a href="https://www.instagram.com/o2o_media/" target="_blank">
                        <svg height="512pt" viewBox="0 0 512 512" width="512pt" xmlns="http://www.w3.org/2000/svg"><path d="m305 256c0 27.0625-21.9375 49-49 49s-49-21.9375-49-49 21.9375-49 49-49 49 21.9375 49 49zm0 0"/><path d="m370.59375 169.304688c-2.355469-6.382813-6.113281-12.160157-10.996094-16.902344-4.742187-4.882813-10.515625-8.640625-16.902344-10.996094-5.179687-2.011719-12.960937-4.40625-27.292968-5.058594-15.503906-.707031-20.152344-.859375-59.402344-.859375-39.253906 0-43.902344.148438-59.402344.855469-14.332031.65625-22.117187 3.050781-27.292968 5.0625-6.386719 2.355469-12.164063 6.113281-16.902344 10.996094-4.882813 4.742187-8.640625 10.515625-11 16.902344-2.011719 5.179687-4.40625 12.964843-5.058594 27.296874-.707031 15.5-.859375 20.148438-.859375 59.402344 0 39.25.152344 43.898438.859375 59.402344.652344 14.332031 3.046875 22.113281 5.058594 27.292969 2.359375 6.386719 6.113281 12.160156 10.996094 16.902343 4.742187 4.882813 10.515624 8.640626 16.902343 10.996094 5.179688 2.015625 12.964844 4.410156 27.296875 5.0625 15.5.707032 20.144532.855469 59.398438.855469 39.257812 0 43.90625-.148437 59.402344-.855469 14.332031-.652344 22.117187-3.046875 27.296874-5.0625 12.820313-4.945312 22.953126-15.078125 27.898438-27.898437 2.011719-5.179688 4.40625-12.960938 5.0625-27.292969.707031-15.503906.855469-20.152344.855469-59.402344 0-39.253906-.148438-43.902344-.855469-59.402344-.652344-14.332031-3.046875-22.117187-5.0625-27.296874zm-114.59375 162.179687c-41.691406 0-75.488281-33.792969-75.488281-75.484375s33.796875-75.484375 75.488281-75.484375c41.6875 0 75.484375 33.792969 75.484375 75.484375s-33.796875 75.484375-75.484375 75.484375zm78.46875-136.3125c-9.742188 0-17.640625-7.898437-17.640625-17.640625s7.898437-17.640625 17.640625-17.640625 17.640625 7.898437 17.640625 17.640625c-.003906 9.742188-7.898437 17.640625-17.640625 17.640625zm0 0"/><path d="m256 0c-141.363281 0-256 114.636719-256 256s114.636719 256 256 256 256-114.636719 256-256-114.636719-256-256-256zm146.113281 316.605469c-.710937 15.648437-3.199219 26.332031-6.832031 35.683593-7.636719 19.746094-23.246094 35.355469-42.992188 42.992188-9.347656 3.632812-20.035156 6.117188-35.679687 6.832031-15.675781.714844-20.683594.886719-60.605469.886719-39.925781 0-44.929687-.171875-60.609375-.886719-15.644531-.714843-26.332031-3.199219-35.679687-6.832031-9.8125-3.691406-18.695313-9.476562-26.039063-16.957031-7.476562-7.339844-13.261719-16.226563-16.953125-26.035157-3.632812-9.347656-6.121094-20.035156-6.832031-35.679687-.722656-15.679687-.890625-20.6875-.890625-60.609375s.167969-44.929688.886719-60.605469c.710937-15.648437 3.195312-26.332031 6.828125-35.683593 3.691406-9.808594 9.480468-18.695313 16.960937-26.035157 7.339844-7.480469 16.226563-13.265625 26.035157-16.957031 9.351562-3.632812 20.035156-6.117188 35.683593-6.832031 15.675781-.714844 20.683594-.886719 60.605469-.886719s44.929688.171875 60.605469.890625c15.648437.710937 26.332031 3.195313 35.683593 6.824219 9.808594 3.691406 18.695313 9.480468 26.039063 16.960937 7.476563 7.34375 13.265625 16.226563 16.953125 26.035157 3.636719 9.351562 6.121094 20.035156 6.835938 35.683593.714843 15.675781.882812 20.683594.882812 60.605469s-.167969 44.929688-.886719 60.605469zm0 0"/></svg>
                    </a>
                    <a href="https://vk.com/o2o.media" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="20.778mm" height="20.778mm" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                             viewBox="0 0 853.2 853.2" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <g id="Слой_x0020_1">
                        <metadata id="CorelCorpID_0Corel-Layer"/>
                        <path class="vk" d="M426.6 0c235.61,0 426.6,191 426.6,426.6 0,235.61 -191,426.6 -426.6,426.6 -235.61,0 -426.6,-191 -426.6,-426.6 0,-235.61 191,-426.6 426.6,-426.6zm257.68 563.75c-0.67,-1.44 -1.3,-2.63 -1.88,-3.6 -9.58,-17.25 -27.91,-38.44 -54.94,-63.56l-0.57 -0.58 -0.3 -0.28 -0.29 -0.29 -0.28 0c-12.27,-11.7 -20.04,-19.57 -23.3,-23.58 -5.95,-7.67 -7.29,-15.44 -4.03,-23.3 2.3,-5.95 10.92,-18.5 25.89,-37.68 7.86,-10.16 14.09,-18.31 18.68,-24.44 33.18,-44.11 47.57,-72.3 43.15,-84.57l-1.7 -2.87c-1.16,-1.72 -4.12,-3.31 -8.91,-4.74 -4.8,-1.44 -10.94,-1.69 -18.41,-0.72l-82.85 0.57c-1.33,-0.46 -3.26,-0.43 -5.74,0.15 -2.5,0.57 -3.75,0.86 -3.75,0.86l-1.43 0.73 -1.14 0.86c-0.97,0.57 -2.02,1.57 -3.17,3.01 -1.14,1.44 -2.1,3.12 -2.86,5.03 -9.03,23.2 -19.27,44.77 -30.79,64.71 -7.1,11.9 -13.62,22.2 -19.56,30.93 -5.95,8.71 -10.94,15.15 -14.95,19.27 -4.04,4.12 -7.67,7.43 -10.94,9.91 -3.28,2.5 -5.76,3.56 -7.48,3.17 -1.73,-0.39 -3.36,-0.78 -4.9,-1.16 -2.69,-1.72 -4.85,-4.07 -6.46,-7.05 -1.64,-2.96 -2.74,-6.71 -3.31,-11.22 -0.57,-4.5 -0.92,-8.38 -1.02,-11.63 -0.09,-3.26 -0.03,-7.88 0.16,-13.81 0.19,-5.95 0.29,-9.98 0.29,-12.08 0,-7.29 0.14,-15.21 0.42,-23.73 0.29,-8.54 0.53,-15.29 0.72,-20.28 0.19,-4.98 0.29,-10.25 0.29,-15.82 0,-5.56 -0.34,-9.91 -1.01,-13.09 -0.66,-3.15 -1.68,-6.22 -3.02,-9.2 -1.35,-2.97 -3.31,-5.27 -5.9,-6.89 -2.59,-1.64 -5.79,-2.93 -9.62,-3.9 -10.17,-2.29 -23.11,-3.53 -38.83,-3.74l-3.58 -0.03 -7.4 0c-29.43,0.18 -48.68,2.5 -57.76,6.94 -4.04,2.11 -7.67,4.99 -10.93,8.62 -3.45,4.22 -3.93,6.52 -1.45,6.91 11.51,1.72 19.65,5.85 24.46,12.36l1.72 3.47c1.34,2.48 2.69,6.9 4.02,13.22 1.35,6.32 2.21,13.32 2.58,20.99 0.96,14 0.96,25.98 0,35.96 -0.94,9.96 -1.86,17.74 -2.72,23.29 -0.86,5.57 -2.16,10.07 -3.88,13.53 -1.72,3.45 -2.88,5.55 -3.47,6.33 -0.57,0.76 -1.06,1.24 -1.43,1.43 -2.5,0.97 -5.08,1.45 -7.77,1.45 -2.67,0 -5.93,-1.35 -9.78,-4.04 -3.83,-2.68 -7.81,-6.37 -11.93,-11.08 -4.11,-4.69 -8.77,-11.26 -13.96,-19.7 -5.17,-8.43 -10.53,-18.39 -16.1,-29.91l-4.6 -8.34c-2.88,-5.37 -6.8,-13.17 -11.79,-23.45 -4.98,-10.26 -9.4,-20.17 -13.22,-29.77 -1.54,-4.01 -3.84,-7.08 -6.91,-9.2l-1.43 -0.86c-0.96,-0.76 -2.5,-1.57 -4.6,-2.44 -2.12,-0.86 -4.31,-1.49 -6.62,-1.86l-78.81 0.57c-8.04,0 -13.52,1.82 -16.4,5.46l-1.15 1.72c-0.56,0.95 -0.85,2.45 -0.86,4.5l0 0.1 0 0.1c0.02,2.11 0.59,4.65 1.72,7.67 11.52,27.03 24.01,53.11 37.54,78.22 13.52,25.13 25.26,45.36 35.22,60.69 9.98,15.35 20.13,29.82 30.49,43.42 10.36,13.62 17.22,22.35 20.56,26.19 3.36,3.84 6,6.7 7.91,8.62l7.19 6.91c4.6,4.6 11.35,10.12 20.29,16.53 8.91,6.43 18.79,12.76 29.61,19 10.84,6.23 23.45,11.31 37.84,15.24 14.38,3.93 28.38,5.52 41.99,4.74l33.08 0c6.71,-0.57 11.79,-2.68 15.24,-6.32l1.14 -1.43c0.77,-1.16 1.49,-2.94 2.15,-5.33 0.67,-2.39 1.02,-5.03 1.02,-7.9 -0.19,-8.25 0.43,-15.67 1.86,-22.29 1.43,-6.61 3.07,-11.58 4.9,-14.95 1.82,-3.36 3.87,-6.19 6.18,-8.48 2.3,-2.31 3.93,-3.69 4.89,-4.17 0.95,-0.48 1.71,-0.81 2.3,-1.02 4.61,-1.53 10.01,-0.03 16.25,4.47 6.23,4.52 12.07,10.06 17.55,16.69 5.47,6.62 12.04,14.05 19.69,22.28 7.69,8.24 14.4,14.38 20.13,18.41l5.76 3.45c3.84,2.31 8.82,4.41 14.96,6.33 6.13,1.93 11.49,2.41 16.1,1.45l73.64 -1.16c7.27,0 12.94,-1.21 16.96,-3.6 4.03,-2.39 6.42,-5.03 7.19,-7.9 0.78,-2.88 0.82,-6.13 0.16,-9.79 -0.69,-3.63 -1.36,-6.19 -2.02,-7.63z"/>
                    </g>
                </svg>
                    </a>
                    <a href="https://www.facebook.com/O2Omedia" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="20.778mm" height="20.778mm"
                             version="1.1"
                             style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                             viewBox="0 0 170.01 170.01" xmlns:xlink="http://www.w3.org/1999/xlink">
                 <defs>
                 </defs>
                            <g id="Слой_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer"/>
                                <path class="facebook"
                                      d="M85.01 0c46.95,0 85.01,38.06 85.01,85.01 0,46.95 -38.06,85.01 -85.01,85.01 -46.95,0 -85.01,-38.06 -85.01,-85.01 0,-46.95 38.06,-85.01 85.01,-85.01zm25.09 90.66l-13.83 0 0 49.32 -20.5 0 0 -49.32 -9.75 0 0 -17.42 9.75 0 0 -11.27c0,-8.06 3.83,-20.68 20.68,-20.68l15.19 0.06 0 16.91 -11.02 0c-1.81,0 -4.35,0.9 -4.35,4.75l0 10.25 15.62 0 -1.79 17.41z"/>
                            </g>
                </svg>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</header>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(55224955, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/55224955" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<?php } ?>