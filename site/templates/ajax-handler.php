<?php

include_once("files/o2o_lk_api/o2o_lk_api.php");


//$calculator_data_path = "/Applications/MAMP/htdocs/site/templates/files/calculator_data.json";
$calculator_data_path = "/home/d/delmir01/O2O/public_html/site/templates/files/calculator_data.json";

function send($data, $success)
{
    echo json_encode(Array("status" => $success ? "ok" : "error", "response" => $data));
}

function get_token($auth_data, $calculator_data_path)
{
    if ($auth_data->access_token == "") {
        $tokens = auth($auth_data->login, $auth_data->password, $auth_data->url);
        $auth_data->access_token = $tokens->access_token;
        $auth_data->refresh_token = $tokens->refresh_token;
        file_put_contents($calculator_data_path, json_encode($auth_data));
    }
    //Чисто как пинг
    $res = get_object_types($auth_data->access_token, $auth_data->url);
    if (strpos($res, "expire") !== false) {
        $tokens = refresh($auth_data->refresh_token, $auth_data->url);
        $auth_data->access_token = $tokens->access_token;
        $auth_data->refresh_token = $tokens->refresh_token;
        file_put_contents($calculator_data_path, json_encode($auth_data));
    }
    return $auth_data->access_token;
}

if ($config->ajax) {
    if ($_REQUEST["type"] == "email") {
        $m = $mail->new();
        $m->from('bot@o2o.media')
            ->fromName("bot O2O");
        $messageBody = "Новая заявка с сайта: \r\n";
        $fields = array("name" => "Имя", "phone" => "Телефон", "email" => "Email", "source" => "Источник", "city" => "Город", "message" => "Сообщение");
        $m->to($page->email);
        if (count($_REQUEST) > 0) {
            foreach ($_REQUEST as $key => $value) {
                if (isset($fields[$key])) {
                    if ($key == "Source") {
                        $m->subject($value);
                    }
                    $messageBody .= $fields[$key] . ": " . $value . "\r\n";
                }
            }
            $messageBody .= "Номер заявки: " . time();
        }
        $m->body = $messageBody;
        if ($m->body != "") {
            if ($m->send() != 0) {
                echo "success";
            } else {
                echo "warning";
            }
        }
    } elseif ($_REQUEST["type"] == "counts") {
        $file_data = json_decode(file_get_contents("./files/cron_data.json"), true);
        unset($file_data['login']);
        unset($file_data['password']);
        unset($file_data['access_token']);
        unset($file_data['refresh_token']);
        unset($file_data['url']);
        echo json_encode($file_data);
    } elseif ($_REQUEST["type"] == "calculate_all") {
        if (!isset($_REQUEST["start"]) || !isset($_REQUEST["end"]) || !isset($_REQUEST["object_ids"])) {
            echo "missing params";
            return;
        }
        $file_data_json = file_get_contents($calculator_data_path);
        if ($file_data_json) {
            $file_data = json_decode($file_data_json);
            if ($file_data->access_token == "") {
                $tokens = auth($file_data->login, $file_data->password, $file_data->url);
                $file_data->access_token = $tokens->access_token;
                $file_data->refresh_token = $tokens->refresh_token;
                file_put_contents($calculator_data_path, json_encode($file_data));
            }
            $res = get_all_counts($file_data->access_token, $file_data->url, $_REQUEST["start"], $_REQUEST["end"], $_REQUEST["object_ids"]);
            if (strpos($res, "expire") !== false) {
                $tokens = refresh($file_data->refresh_token, $file_data->url);
                $file_data->access_token = $tokens->access_token;
                $file_data->refresh_token = $tokens->refresh_token;
                file_put_contents($calculator_data_path, json_encode($file_data));
                $res = get_all_counts($file_data->access_token, $file_data->url, $_REQUEST["start"], $_REQUEST["end"], $_REQUEST["object_ids"]);
            }
            echo $res;
        }
    } elseif ($_REQUEST["type"] == "calculate_objects") {
        if (!isset($_REQUEST["start"]) || !isset($_REQUEST["end"]) || !isset($_REQUEST["object_ids"])) {
            echo "missing params";
            return;
        }
        $file_data_json = file_get_contents($calculator_data_path);
        if ($file_data_json) {
            $file_data = json_decode($file_data_json);
            if ($file_data->access_token == "") {
                $tokens = auth($file_data->login, $file_data->password, $file_data->url);
                $file_data->access_token = $tokens->access_token;
                $file_data->refresh_token = $tokens->refresh_token;
                file_put_contents($calculator_data_path, json_encode($file_data));
            }
            $res = get_data_counts($file_data->access_token, $file_data->url, $_REQUEST["start"], $_REQUEST["end"], $_REQUEST["object_ids"]);
            if (strpos($res, "expire") !== false) {
                $tokens = refresh($file_data->refresh_token, $file_data->url);
                $file_data->access_token = $tokens->access_token;
                $file_data->refresh_token = $tokens->refresh_token;
                file_put_contents($calculator_data_path, json_encode($file_data));
                $res = get_data_counts($file_data->access_token, $file_data->url, $_REQUEST["start"], $_REQUEST["end"], $_REQUEST["object_ids"]);
            }
            echo $res;
        }
    } elseif ($_REQUEST["type"] == "send_brief") {
        if (!isset($_REQUEST["login"]) || !isset($_REQUEST["name"]) || !isset($_REQUEST["inn"])
            || !isset($_REQUEST["address"]) || !isset($_REQUEST["brief_data"]) || !isset($_REQUEST["sum"])) {
            echo "missing params";
            return;
        }
        $file_data_json = file_get_contents($calculator_data_path);
        if ($file_data_json) {
            $file_data = json_decode($file_data_json);
            $access_token = get_token($file_data, $calculator_data_path);
            $tok = get_tmp_token($_REQUEST["login"], $access_token, $file_data->url);
            if ($tok->code == 401) {
                $user_crt_res = registr_user($_REQUEST["login"], $access_token, $file_data->url);
                if ($user_crt_res->code != 200) {
                    send("внутренняя ошибка: can't create user", false);
                    return;
                }
                $tok = get_tmp_token($_REQUEST["login"], $access_token, $file_data->url);
                if ($tok->code == 401) {
                    send("внутренняя ошибка: can't get token", false);
                    return;
                }
            }
            $tmp_token = $tok->response->access_token;
            if (strlen($tmp_token) == 0) {
                send("внутренняя ошибка: can't get token", false);
                return;
            }
            $cr_pr = create_payments_requisites($_REQUEST["name"], $_REQUEST["kpp"], $_REQUEST["address"], $_REQUEST["inn"], $tmp_token, $file_data->url);
            if ($cr_pr->code != 200 || !is_int($cr_pr->response)) {
                send(sprintf("внутренняя ошибка: can't create payment requisites %s", json_encode($cr_pr->response)), false);
                return;
            }
            $cr_ord = create_order($_REQUEST["brief_data"], $_REQUEST["sum"], $cr_pr->response, $tmp_token, $file_data->url, isset($_REQUEST["coupon_name"]) ? $_REQUEST["coupon_name"] : "");
            if ($cr_ord->code != 200 || !is_int($cr_ord->response->order_id)) {
                send(sprintf("внутренняя ошибка: can not create order %s ", json_encode($cr_ord->response)), false);
                return;
            }
            $invoice = get_invoice($cr_ord->response->invoice_id, $tmp_token, $file_data->url);
            send($invoice, true);
        }
    } elseif ($_REQUEST["type"] == "owner_info") {
        if (!isset($_REQUEST["object_id"])) {
            echo "missing params";
            return;
        }
        $file_data_json = file_get_contents($calculator_data_path);
        if ($file_data_json) {
            $file_data = json_decode($file_data_json);
            $access_token = get_token($file_data, $calculator_data_path);
            $cr_pr = get_owner_info($_REQUEST["object_id"], $access_token, $file_data->url);
            if ($cr_pr->code != 200 && $cr_pr->code != 204) {
                send(sprintf("внутренняя ошибка: can't get owner info %s", json_encode($cr_pr->response)), false);
                return;
            }
            send($cr_pr, true);
        }
    } elseif ($_REQUEST["type"] == "coupon") {
        if (!isset($_REQUEST["coupon_name"])) {
            echo "missing params";
            return;
        }
        $file_data_json = file_get_contents($calculator_data_path);
        if ($file_data_json) {
            $file_data = json_decode($file_data_json);
            $access_token = get_token($file_data, $calculator_data_path);
            $cr_pr = get_coupon_by_name($_REQUEST["coupon_name"], $access_token, $file_data->url);
            if ($cr_pr->code != 200) {
                send(sprintf("внутренняя ошибка: can't get coupon %s", json_encode($cr_pr->response)), false);
                return;
            }
            send($cr_pr, true);
        }
    }
} else {
    $session->redirect("/");
}