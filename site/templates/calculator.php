<?php namespace ProcessWire; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width" id="viewport">
    <title>O2O calculator — расчёт стоимости маркетинговых услуг</title>
    <meta name="description" content="Узнайте стоимость рекламной кампании и оформите заказ с помощью онлайн-калькулятора">
    <meta property="og:title" content="O2O calculator — расчёт стоимости маркетинговых услуг" />
    <meta property="og:description" content="Узнайте стоимость рекламной кампании и оформите заказ с помощью онлайн-калькулятора" />
    <meta property="og:image" content="https://o2o.media/site/templates/img/media.png" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://o2o.media/calculator" />
    <meta property="fb:app_id" content="612853856119216" />
    <link rel="shortcut icon" href="<?= $config->urls->templates ?>img/o2o_favicon.png" type="image/png">
    <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/fonts.css">
    <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/calculator.14.css">
    <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/jquery.modal.min.css">
    <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/datepicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/select2.min.css">
<!--<link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/firebug.css">-->
    <script src="https://api-maps.yandex.ru/2.1/?apikey=85fde534-aeff-44b9-86b9-8db26efca963&lang=ru_RU" type="text/javascript"></script>
    <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/jquery.modal.min.js"></script>
    <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/datepicker.min.js"></script>
    <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/datepicker.ru.js"></script>
    <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/moment.min.js"></script>
    <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/select2.min.js"></script>
    <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/html2canvas.js"></script>
    <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/detect.min.js"></script>
<!-- <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/firebug.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
    <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/index.37.min.js"></script>
    <script src="//code.jivosite.com/widget.js" data-jv-id="nVf5AT60uk" async></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-157960803-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-157960803-1');
    </script>
</head>
<body class="page page-calc">
<style>
.ui-dialog-titlebar-close .ui-icon {
    background: url(<?= $config->urls->templates ?>img/close.svg) center/contain no-repeat !important;
}
</style>
<header class="header">
    <div class="container row">
        <a href="https://o2o.media"><img class="header-logo" src="<?= $config->urls->templates ?>img/logo-calc.png" alt="Логотип O2O.calculator"></a>
        <div class="header-phone">
            <a class="tel:+78002503452">8-800-250-34-52</a>
        </div>
        <button type="button" class="burger">
            <div></div>
            <div></div>
            <div></div>
        </button>
        <ul class="header-menu header-mobile-menu">
            <li class="header-menu__item"><a href="https://o2o.media">Сайт O2O.media</a></li>
            <li class="header-menu__item"><a href="https://o2o.digital">Сайт O2O.digital</a></li>
            <li class="header-menu__item contacts">
                <a href="tel:+78002503452" class="contacts__phone">8-800-250-34-52</a>
            </li>
        </ul>
    </div>
</header>

<main class="main">
    <section class="section-brief" style="opacity: 0">
        <div class="container row">
            <div class="brief-container">
                <div class="briefs">
                    <div class="briefs__container owl-carousel owl-theme owl-carousel-calc">
                        <div class="brief item map visited" data-step="1">
                            <h3>Хотите разместить рекламу на рекламной конструкции?</h3>
                            <p>Выберите одну или несколько конструкций.<br>Если вас не интересует это предложение, просто пропустите этот шаг.</p>
                            <div class="row">
                                <div class="brief-block">
                                    <div class="key">Город</div>
                                    <div class="value">
                                        <div class="select-custom" id="select-map-city">
                                            <span class="select-custom__span">
                                                <span class="select-custom__arrow"></span>
                                                <span class="select-custom__option">
                                                    <span>Не выбран</span>
                                                </span>
                                            </span>
                                            <select class="select__default"></select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row map-row" style="margin-top: 30px; display: flex; flex-wrap: wrap">
                                <div id="map"></div>
                                <div class="map-info">
                                    <div class="row">
                                        <div class="total-selected">Выбрано конструкций: <span class="count">0</span></div>
                                    </div>
                                    <ul class="row list-selected-objects"></ul>
                                    <div class="row">
                                        <button class="btn-transparent btn-remove-all">Очистить всё</button>
                                    </div>
                                </div>
                            </div>
                            <p class="map-hint">* на карте указаны данные за <span class="month"></span></p>
                        </div>
                        <div class="brief item map" data-step="2">
                            <h3>Хотите использовать данные, собранные системой О2О?</h3>
                            <p>Выберите одну или несколько конструкций.<br>Если вас не интересует это предложение, просто пропустите этот шаг.</p>
                            <div class="row">
                                <div class="brief-block">
                                    <div class="key">Город</div>
                                    <div class="value">
                                        <div class="select-custom" id="select-map-city">
                                            <span class="select-custom__span">
                                                <span class="select-custom__arrow"></span>
                                                <span class="select-custom__option">
                                                    <span>Не выбран</span>
                                                </span>
                                            </span>
                                            <select class="select__default">

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="brief-block select-map-city">
                                    <div class="key">Тип объекта</div>
                                    <div class="value">
                                        <div class="select-custom" id="select-map-type-object">
                                            <span class="select-custom__span">
                                                <span class="select-custom__arrow"></span>
                                                <span class="select-custom__option">
                                                    <span>Не выбран</span>
                                                </span>
                                            </span>
                                            <select class="select__default">

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="brief-block map-period">
                                    <div class="key">Период</div>
                                    <div class="value">
                                        <input type='text'
                                               placeholder="дд.мм.гггг - дд.мм.гггг"
                                               data-range="true"
                                               data-language="ru"
                                               data-field="period"
                                               data-multiple-dates-separator=" - " />
                                    </div>
                                </div>
                            </div>
                            <div class="row loader-wrapper hidden map-row" style="margin-top: 30px; display: flex; flex-wrap: wrap">
                                <div id="map2"></div>
                                <img class="loader" src="<?= $config->urls->templates ?>img/loader.svg" alt="">
                                <div class="map-info">
                                    <div class="row">
                                        <div class="count-all-macs">Кол-во собранных данных за указанный период: <div class="count">0</div></div>
                                    </div>
                                    <div class="row">
                                        <div class="total-selected">Выбрано конструкций: <span class="count">0</span></div>
                                    </div>
                                    <ul class="row list-selected-objects"></ul>
                                    <div class="row">
                                        <button class="btn-transparent btn-remove-all">Очистить всё</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="brief item" data-step="3">
                            <h3>На каких рекламных площадках вы хотели бы<br>показать рекламное объявление?</h3>
                            <p>Выберите один или несколько вариантов</p>
                            <div class="brief-multi">
                                <label class="cont">Довериться специалистам О2О
                                    <input type="checkbox" name="adv-platform" id="adv-platform-trust" data-field="platforms_auto">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="cont" style="display: flex; align-items: center" data-hide>Яндекс (по mac-адресам)
                                    <div class="helper" title="Позволит охватить пользователей в сервисах, таких как яндекс почта, дром.ру, яндекс недвижимость, под любую сферу деятельности у яндекс существуют сайты- партнеры, которые позволяют показывать баннеры на их ресурсах"></div>
                                    <input type="checkbox" name="adv-platform" data-answer="yandex" data-field="platforms_yandex">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="cont" style="display: flex; align-items: center" data-hide>MyTarget (по mac-адресам)
                                    <div class="helper" title="Показываем рекламу в самых популярных соц-сетях СНГ, таких как вконтакте, одноклассники, а так же на сайтах-партнерах mail.ru"></div>
                                    <input type="checkbox" name="adv-platform" data-answer="mytarget" data-field="platforms_my_target">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="cont" style="display: flex; align-items: center" data-hide>Facebook
                                    <div class="helper" title="Показываем рекламу в Facebook и Instagram"></div>
                                    <input type="checkbox" name="adv-platform" data-answer="facebook" data-field="platforms_facebook">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="cont" style="display: flex; align-items: center" data-hide>Google
                                    <input type="checkbox" name="adv-platform" data-answer="google" data-field="platforms_google">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="cont" style="display: flex; align-items: center" data-hide>YouTube
                                    <input type="checkbox" name="adv-platform" data-answer="youtube" data-field="platforms_youtube">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="brief item" data-step="4">
                            <h3>Какая целевая аудитория вас интересует?</h3>
                            <div class="row">
                                <div class="brief-block">
                                    <div class="key">Нужна помощь с определением целевой аудитории?
                                        <div class="helper" title="Цель любой рекламной кампании, точно показать баннер аудитории, которая вероятнее всего приобретет Ваш товар/услугу. Для лучшей эффективности, опишите целевой портрет Вашего клиента. Если необходима помощь с определением Вашей целевой аудитории, доверьтесь нашим специалистам по маркетингу"></div>
                                    </div>
                                    <div class="value">
                                        <label class="cont">Да
                                            <input type="radio" name="ca-trust" class="ca-trust" data-answer="yes" data-field="ca_trust">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="cont">Нет
                                            <input type="radio" name="ca-trust" class="ca-trust" data-answer="no" data-field="ca_trust">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="flex-wrap: wrap" data-hide>
                                <div class="brief-block">
                                    <div class="key">Пол</div>
                                    <div class="value">
                                        <label class="cont">Мужчины
                                            <input type="checkbox" name="gender" data-answer="male" data-field="ta_gender">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="cont">Женщины
                                            <input type="checkbox" name="gender" data-answer="female" data-field="ta_gender">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="brief-block">
                                    <div class="key">Возраст</div>
                                    <div class="value">
                                        <div class="slider-range" id="slider-age">
                                            <div id="custom-handle-1" class="ui-slider-handle"></div>
                                            <div id="custom-handle-2" class="ui-slider-handle"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="brief-block">
                                    <div class="key">Уровень дохода</div>
                                    <div class="value">
                                        <div class="slider-range" id="slider-income">
                                            <div id="custom-handle-3" class="ui-slider-handle"></div>
                                            <div id="custom-handle-4" class="ui-slider-handle"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row brief-geolocation" data-hide>
                                <div class="brief-block">
                                    <div class="key">Геолокация</div>
                                    <div class="value">
                                        <div class="brief-geolocation__cities">
                                            <a class="city add-city" href="#modal-add-cities" rel="modal:open"></a>

                                            <div id="modal-add-cities" class="modal">
                                                <form class="modal__form">
                                                    <div class="modal__form-row">
                                                        <select class="select-cities" name="state"></select>
                                                    </div>
                                                    <div class="modal__form-row">
                                                        <a class="btn-primary" href="#" rel="modal:close">Добавить</a>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" data-hide>
                                <div class="brief-block" style="width: 100%">
                                    <div class="key">Дополнительные критерии</div>
                                    <div class="value">
                                        <textarea name="additional-info" data-field="ta_additional_info"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="brief item" data-step="5">
                            <h3>Информация о сайте</h3>
                            <div class="row">
                                <div class="brief-block">
                                    <div class="key">Есть ли у Вас сайт?</div>
                                    <div class="value">
                                        <div style="float: left">
                                            <label class="cont">Да
                                                <input type="radio" name="has-site" data-answer="yes" data-field="site_need">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div style="float: left; margin-left: 23px">
                                            <label class="cont">Нет
                                                <input type="radio" name="has-site" data-answer="no" data-field="site_need">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="brief-block price-site">
                                    <div class="key">Создать сайт</div>
                                    <div class="value">
                                        <div style="float: left">
                                            <label class="cont">Landing page
                                                <input type="radio" name="price-site" data-answer="50000" data-field="site_type">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div style="float: left; margin-left: 23px">
                                            <label class="cont">Многостраничный сайт
                                                <input type="radio" name="price-site" data-answer="100000" data-field="site_type">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div style="float: left; margin-left: 23px">
                                            <label class="cont">Интернет-магазин
                                                <input type="radio" name="price-site" data-answer="300000" data-field="site_type">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="brief item" data-step="6">
                            <h3>Подключена и настроена ли на вашем сайте аналитика?</h3>
                            <p>Выберите один или несколько вариантов</p>
                            <div class="row">
                                <div class="brief-block type-site disabled">
                                    <div class="key">Вид сайта
                                        <div class="helper" title="Давайте для начала определимся с форматом сайта – простой одностраничный сайт сделанный на конструкторе, по которому нашим специалистам будет не сложно подключить всю систему аналитики или большой многостраничный, написанный вручную программистом, где нам потребуется чуть больше времени на внедрение этих систем"></div>
                                    </div>
                                    <div class="value">
                                        <div class="view-page" style="float: left">
                                            <label class="cont">Одностраничный сайт
                                                <input type="radio" name="targets" data-answer="single-page" data-field="site_count_page">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="view-page" style="float: left; margin-left: 23px">
                                            <label class="cont">Многостраничный сайт
                                                <input type="radio" name="targets" data-answer="multi-page" data-field="site_count_page">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="brief-block">
                                    <div class="key">Установлена ли на сайте система аналитики рекламных кампаний?</div>
                                    <div class="value">
                                        <label class="cont disabled">Да
                                            <input type="radio" name="system-analytics" data-answer="yes" data-field="site_need_help">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="cont">Нет
                                            <input type="radio" name="system-analytics" data-answer="no" data-field="site_need_help">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="brief-block">
                                    <div class="key margin0">
                                        <label class="cont"><span class="word">Подключить GoogleAnalytics</span>
                                            <input type="checkbox" name="ga" data-field="site_google">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="brief-block">
                                    <div class="key margin0">
                                        <label class="cont"><span class="word">Подключить Яндекс Метрику</span>
                                            <input type="checkbox" name="yam" data-field="site_yandex">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="brief-block">
                                    <div class="key margin0">
                                        <label class="cont" style="display: flex; align-items: center"><span class="word">Расставить цели в метрике</span>
                                            <div class="helper" title="Когда кто-то оставляет заявку/покупает товар с Вашего сайта, на который перешли по нашей рекламе, мы сможем отследить, что реклама работает"></div>
                                            <input type="checkbox" name="targets" data-field="site_targets">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="brief-block">
                                    <div class="key margin0">
                                        <label class="cont" style="display: flex; align-items: center"><span class="word">Подключить коллтрекинг</span>
                                            <div class="helper" title="Важно отслеживать не только количество заявок с сайта, но и количество звонков с сайта"></div>
                                            <input type="checkbox" name="calltracking" data-field="site_call_tracking">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="brief item" data-step="7">
                            <h3>Расчёт стоимости рекламной кампаниии</h3>
                            <div class="row">
                                <div class="brief-block">
                                    <div class="key">Создание уникальной креативной кампании
                                         <div class="helper" title="Реализация в формате видеопродукта 3 шт: полнометражныый видеоролик продолжительностью 1 мин, видеоролик для соц. сетей продолжительностью  30 сек, видеоролик для наружной рекламы продолжительностью  10 сек."></div>
                                    </div>
                                    <div class="value">
                                        <div style="float: left">
                                            <label class="cont">Довериться специалистам О2О
                                                <input type="checkbox" name="create-company" data-field="banners_creative_company">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row block-design">
                                <div class="brief-block has-creatives">
                                    <div class="key">Есть ли у вас макеты рекламных баннеров?</div>
                                    <div class="value">
                                        <div style="float: left" class="float-none">
                                            <label class="cont wdth-mobile-100 mrg-btm-15">Да
                                                <input type="radio" name="layouts" data-answer="yes" data-field="banners_need_adaptation">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div style="float: left; margin-left: 50px; width: 290px" class="float-none">
                                            <label class="cont wdth-mobile-100 mrg-btm-15">Да, но нужна адаптация
                                                <input type="radio" name="layouts" data-answer="yes-no" data-field="banners_need_adaptation">
                                                <span class="checkmark"></span>
                                            </label>
                                            <div class="count-pages count-layouts">
                                                Кол-во макетов
                                                <div class="count-pages__select">
                                                    <div class="select-custom">
                                                        <span class="select-custom__span">
                                                            <span class="select-custom__arrow"></span>
                                                            <span class="select-custom__option">
                                                                <span>1</span>
                                                            </span>
                                                        </span>
                                                        <select class="select__default" data-field="select">
                                                            <option class="select__option" value="1">1</option>
                                                            <option class="select__option" value="2">2</option>
                                                            <option class="select__option" value="3">3</option>
                                                            <option class="select__option" value="4">4</option>
                                                            <option class="select__option" value="5">5</option>
                                                            <option class="select__option" value="6">6</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="float: left" class="float-none">
                                            <label class="cont wdth-mobile-100">Нет
                                                <input type="radio" name="layouts" data-answer="no" data-field="banners_need_adaptation">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row support-designer block-design">
                                <div class="brief-block">
                                    <div class="key">Нужна помощь дизайнера
                                        <div class="helper" title="Существует два вида рекламных баннеров, первый это шаблонный (фон, надпись и кнопка призыва к действию). Второй же вариант позволит запустить в рекламу уникальный баннер, созданный нашими дизайнерами-профессионалами, привлекающий большее внимание"></div>
                                    </div>
                                    <div class="value">
                                        <div style="float: left">
                                            <label class="cont mrg-btm-15">Шаблонный макет
                                                <input type="radio" name="support" data-answer="template" data-field="banners_template">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div style="margin-left: 23px; float: left;">
                                            <label class="cont" style="margin-bottom: 5px">Уникальный макет
                                                <input type="radio" name="support" data-answer="unique" data-field="banners_template">
                                                <span class="checkmark"></span>
                                            </label>
                                            <div class="count-pages count-creatives">
                                                Кол-во креативов
                                                <div class="count-pages__select">
                                                    <div class="select-custom">
                                                        <span class="select-custom__span">
                                                            <span class="select-custom__arrow"></span>
                                                            <span class="select-custom__option">
                                                                <span>1</span>
                                                            </span>
                                                        </span>
                                                        <select class="select__default" data-field="select">
                                                            <option class="select__option" value="1">1</option>
                                                            <option class="select__option" value="2">2</option>
                                                            <option class="select__option" value="3">3</option>
                                                            <option class="select__option" value="4">4</option>
                                                            <option class="select__option" value="5">5</option>
                                                            <option class="select__option" value="6">6</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row block-design">
                                <label class="cont" style="display: flex; align-items: center">Создание ролика
                                    <div class="helper" title="Motion-design ролик (несложные, но эффектные анимации, без использования натурных съёмок и 3D графики, до 15 секунд)"></div>
                                    <input type="checkbox" name="create-video" data-field="banners_video">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="row">
                                <div class="brief-block count-shows">
                                    <div class="key">Количество показов
                                        <div class="helper" title="Количество показов рекламного объявления – чем больше мы покажем нашу рекламу, тем больше людей смогут заинтересоваться Вашим товаром/услугой"></div>
                                    </div>
                                    <div class="value">
                                        <div class="count-pages__select">
                                            <div class="select-custom">
                                                        <span class="select-custom__span">
                                                            <span class="select-custom__arrow"></span>
                                                            <span class="select-custom__option">
                                                                <span>10 000</span>
                                                            </span>
                                                        </span>
                                                <select class="select__default" data-field="select">
                                                    <option class="select__option" value="10000">10 000</option>
                                                    <option class="select__option" value="100000">100 000</option>
                                                    <option class="select__option" value="200000">200 000</option>
                                                    <option class="select__option" value="300000">300 000</option>
                                                    <option class="select__option" value="400000">400 000</option>
                                                    <option class="select__option" value="500000">500 000</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="brief-block">
                                    <div class="key">За какой промежуток времени необходимо<br>использовать всё количество показов?
                                        <div class="helper" title="Все зависит от цели рекламной кампании, если есть необходимость показать рекламу всем пользователям в ограниченный срок, это увеличит Ваши расходы на рекламу"></div>
                                    </div>
                                    <div class="value">
                                        <label class="cont mrg-btm-15">1 неделя
                                            <input type="radio" name="interval" data-answer="1-week" data-field="shows_runtime_weeks">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="cont mrg-btm-15">2 недели
                                            <input type="radio" name="interval" data-answer="2-week" data-field="shows_runtime_weeks">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="cont">1 месяц
                                            <input type="radio" name="interval" data-answer="1-month" data-field="shows_runtime_weeks">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="brief item" data-step="8">
                            <h3>Нужна помощь с социальными сетями?</h3>
                            <div class="row" style="flex-wrap: wrap">
                                <div class="block-social" data-name="vk">
                                    <div class="block-social__name">Вконтакте</div>
                                    <div class="block-social__multi">
                                        <label class="cont">Создать
                                            <input type="checkbox" name="create" data-field="smm">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="cont">Ведение
                                            <input type="checkbox" name="vedenie" data-field="smm">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="cont">Оформление
                                            <input type="checkbox" name="decor" data-field="smm">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="block-social" data-name="facebook">
                                    <div class="block-social__name">Facebook</div>
                                    <div class="block-social__multi">
                                        <label class="cont">Создание
                                            <input type="checkbox" name="create" data-field="smm">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="cont">Ведение
                                            <input type="checkbox" name="vedenie" data-field="smm">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="cont">Оформление
                                            <input type="checkbox" name="decor" data-field="smm">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="block-social" data-name="inst">
                                    <div class="block-social__name">Instagram</div>
                                    <div class="block-social__multi">
                                        <label class="cont">Создание
                                            <input type="checkbox" name="create" data-field="smm">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="cont">Ведение
                                            <input type="checkbox" name="vedenie" data-field="smm">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="cont">Оформление
                                            <input type="checkbox" name="decor" data-field="smm">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="brief-block">
                                    <div class="key">Нужна ли помощь с созданием контента для соц. сетей?</div>
                                    <div class="value">
                                        <label class="cont">Да
                                            <input type="radio" name="support-content" class="support-content" data-answer="yes" data-field="smm">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="cont">Нет
                                            <input type="radio" name="support-content" class="support-content" data-answer="no" data-field="smm">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="brief-block block-reputation">
                                    <div class="key">Нужна ли помощь с управлением репутацией?</div>
                                    <div class="value">
                                        <label class="cont">Устранение негатива, размещение отзывов, создание положительного инфополя
                                            <input type="checkbox" data-answer="negative" data-field="serm">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="cont">Ответы на отзывы от лица бренда
                                            <input type="checkbox" data-answer="reviews" data-field="serm">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="brief-result">
                    <div class="brief-result__container">
                        <div class="brief-order__container">
                            <h3>
                                Расчет стоимости рекламной кампании
                                <img class="get-pdf" src="<?= $config->urls->templates ?>img/pdf.svg" alt="">
                            </h3>
                            <div class="brief-order"></div>
                            <div class="coupon-final">
                                <div class="coupon" id="coupon">
                                    <input class="coupon-input" placeholder="Купон">
                                    <button class="btn-transparent" id="apply-coupon">Применить</button>
                                </div>
                                <div class="final">
                                    <label>Итоговая стоимость<br>рекламной кампании</label>
                                    <p class="hint" style="display: none">(включая услуги по настройке<br>и рекомендуемый бюджет)</p>
                                    <div class="total">
                                        <span>Итого</span>
                                        <span class="old-sum"></span>
                                        <span class="sum brief-result__price" id="final-price"><span>0</span> ₽</span>
                                    </div>
                                    <button class="btn-primary checkout" id="checkout">Оформить заказ</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="brief-footer" id="brief-footer">
                    <div class="brief-footer__left">
                        <div class="steps-container">
                            <ul class="brief-steps">
                                <li class="brief-step active gradient action visited" data-step="1">
                                    <div class="circle"></div>
                                    <span class="step-count">Шаг 1</span>
                                </li>
                                <li class="brief-step action" data-step="2">
                                    <div class="circle"></div>
                                    <span class="step-count">Шаг 2</span>
                                </li>
                                <li class="brief-step action" data-step="3">
                                    <div class="circle"></div>
                                    <span class="step-count">Шаг 3</span>
                                </li>
                                <li class="brief-step action" data-step="4">
                                    <div class="circle"></div>
                                    <span class="step-count">Шаг 4</span>
                                </li>
                                <li class="brief-step action" data-step="5">
                                    <div class="circle"></div>
                                    <span class="step-count">Шаг 5</span>
                                </li>
                                <li class="brief-step action" data-step="6">
                                    <div class="circle"></div>
                                    <span class="step-count">Шаг 6</span>
                                </li>
                                <li class="brief-step action" data-step="7">
                                    <div class="circle"></div>
                                    <span class="step-count">Шаг 7</span>
                                </li>
                                <li class="brief-step action" data-step="8">
                                    <div class="circle"></div>
                                    <span class="step-count">Шаг 8</span>
                                </li>
                                <li class="brief-step action" data-step="9">
                                    <div class="circle"></div>
                                    <span class="step-count">Шаг 9</span>
                                </li>
                            </ul>
                        </div>
                        <div class="brief-actions">
                            <button class="btn-primary disabled action" data-direction="prev" data-step="0">Назад</button>
                            <button class="btn-primary action" data-direction="next" data-step="2">Далее</button>
                        </div>
                    </div>
                    <div class="brief-footer__right">
                        <button class="btn-transparent btn-connect">Запросить консультацию</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<footer class="footer">
</footer>


<div class="modal-wrapper modal-wrapper-connection">
    <div class="dialog-connection modal">
        <h2 class="modal__caption">
            Оставить заявку
        </h2>
        <p class="modal__description">
            Оставьте свои данные и наши менеджеры свяжутся с вами в ближайшее время
        </p>
        <form class="modal__form">
            <div class="modal__form-row">
                <label>
                    Имя<span class="important"></span>
                    <input type="text" name="name" required>
                </label>
            </div>
            <div class="modal__form-row">
                <label>
                    Телефон<span class="important"></span>
                    <input type="text" name="phone" required>
                </label>
            </div>
            <div class="modal__form-row">
                <label>
                    E-mail<span class="important"></span>
                    <input type="email" name="email" required>
                </label>
            </div>
            <input type="hidden" name="source" value="Запросить консультацию (калькулятор)">
            <input type="hidden" name="type" value="email">
            <button class="btn-primary" type="submit">Отправить</button>
        </form>
        <div class="modal__hint">
            Поля помеченные * обязательны к заполнению. Нажимая на кнопку «Отправить»,<br>вы соглашаетесь с <a href="<?= $config->urls->templates ?>privacy.pdf" target="_blank">политикой обработки конфиденциальных данных</a>.
        </div>
    </div>
</div>

<div class="modal-wrapper modal-wrapper-params">
    <div class="dialog-params modal">
        <h2 class="modal__caption">
            Обратная связь
        </h2>
        <p class="modal__description">
            Оставьте свои данные для обратной связи
        </p>
        <form class="modal__form form-params">
            <div class="modal__form-row">
                <label>
                    E-mail<span class="important"></span>
                    <input type="email" name="login" required>
                </label>
            </div>
            <div class="modal__form-row">
                <label>
                    Телефон<span class="important"></span>
                    <input type="text" name="phone" required>
                </label>
            </div>
            <input type="hidden" name="type" value="email">
            <button class="btn-primary btn-params" type="submit">Отправить</button>
        </form>
        <div class="modal__hint">
            Поля помеченные * обязательны к заполнению. Нажимая на кнопку «Отправить»,<br>вы соглашаетесь с <a href="<?= $config->urls->templates ?>privacy.pdf" target="_blank">политикой обработки конфиденциальных данных</a>.
        </div>
    </div>
</div>

<div class="modal-wrapper modal-wrapper-invoice">
    <div class="dialog-invoice modal">
        <h2 class="modal__caption">
            Выставление счёта
        </h2>
        <p class="modal__description">
            Оставьте свои данные для выставления счёта
        </p>
        <form class="modal__form form-invoice">
            <input type="hidden" name="login" value="">
            <div class="modal__form-row">
                <label>
                    <span>Наименование<span class="important"></span><br>плательщика</span>
                    <input type="text" name="name" required>
                </label>
            </div>
            <div class="modal__form-row">
                <label>
                    <span>Юридический<span class="important"></span><br>адрес</span>
                    <input type="text" name="address" required>
                </label>
            </div>
            <div class="modal__form-row">
                <label>
                    ИНН<span class="important"></span>
                    <input type="text" name="inn" required>
                </label>
            </div>
            <div class="modal__form-row">
                <label>
                    КПП
                    <input type="text" name="kpp">
                </label>
            </div>
            <input type="hidden" name="type" value="send_brief">
            <button class="btn-primary btn-invoiced" type="submit">Отправить</button>
        </form>
        <div class="modal__hint">
            Поля помеченные * обязательны к заполнению. Нажимая на кнопку «Отправить»,<br>вы соглашаетесь с <a href="<?= $config->urls->templates ?>О2О_Договор_оказания_услуг_калькулятор.pdf" target="_blank">Договором оферты</a>
        </div>
    </div>
</div>

<div class="modal-wrapper modal-wrapper-invoice-html">
    <div class="dialog-invoice-html modal">
        <div id="invoice-print"></div>
        <iframe class="invoice-html__content iframe-invoice"></iframe>
    </div>
</div>

<div class="modal-wrapper modal-wrapper-notification">
    <div class="dialog-notification modal">
        <h2 class="modal__caption">Повторно ознакомиться<br>с выставленным счётом<br>Вы можете в <a href="https://lk.o2o.media" target="_blank">личном кабинете</a></h2>
        <p class="modal__description">
            Для получения доступа к личному кабинету (при отсутствии аккаунта) Вам необходимо перейти по ссылке из письма, которое было отправлено на указанную почту
        </p>
    </div>
</div>

<script>
    const viewport_meta = document.getElementById('viewport');
    const viewports = {
        default: viewport_meta.getAttribute('content'),
        landscape: 'width=1280'
    };

    const viewport_set = function() {
        if (screen.width <= 640)
            viewport_meta.setAttribute('content', viewports.default);
        else
            viewport_meta.setAttribute('content', viewports.landscape);
    };

    viewport_set();

    window.onresize = function() {
        viewport_set();
    }
</script>

</body>
</html>