<?php

namespace ProcessWire;
include_once("o2o_lk_api/o2o_lk_api.php");

$current_month = date('my');
$first_minute = mktime(0, 0, 0, date("n"), 1);
$last_minute = mktime(23, 59, 59, date("n"), date("t"));
//$file_data_json = file_get_contents("/var/www/html/o2o/site/templates/files/cron_data.json");
$file_data_json = file_get_contents("/home/d/delmir01/O2O/public_html/site/templates/files/cron_data.json");
if ($file_data_json) {
    $file_data = json_decode($file_data_json, true);
    if ($file_data["access_token"] == "") {
        $tokens = auth($file_data["login"], $file_data["password"], $file_data["url"]);
        $file_data["access_token"] = $tokens->access_token;
        $file_data["refresh_token"] = $tokens->refresh_token;
        file_put_contents("/home/d/delmir01/O2O/public_html/site/templates/files/cron_data.json", json_encode($file_data));
    }

    $cities = get_cities($file_data["access_token"], $file_data["url"]);

    if (strpos($cities, "expire") !== false) {
        $tokens = refresh($file_data["refresh_token"], $file_data["url"]);
        $file_data["access_token"] = $tokens->access_token;
        $file_data["refresh_token"] = $tokens->refresh_token;
        file_put_contents("/home/d/delmir01/O2O/public_html/site/templates/files/cron_data.json", json_encode($file_data));
        return;
    }

    $all = 0;
    $all_objects = get_objects($file_data["access_token"], $file_data["url"]);
    $object_ids = array();
    foreach (json_decode($all_objects)->response as $obj) {
        $object_ids[] = $obj->id;
    }
    $all = get_all_counts($file_data["access_token"], $file_data["url"], "1551398400", "4076006400", $object_ids);
    $file_data["all"] = json_encode(json_decode($all)->response);

    $object_types = get_object_types($file_data["access_token"], $file_data["url"]);
    if (strpos($object_types, "response") !== false) {
        $file_data["object_types"] = json_decode($object_types)->response;
    }

    foreach (json_decode($cities)->response as $city) {
        $objects = json_decode(get_objects($file_data["access_token"], $file_data["url"], $city->id))->response;
        $object_ids = array();
        foreach ($objects as $obj) {
            $object_ids[] = $obj->id;
            unset($obj->created_at);
            unset($obj->updated_at);
            unset($obj->deleted_at);
            unset($obj->sputnik_id);
            unset($obj->period_supplier_id);
            unset($obj->screen_external_id);
        }
        $file_data["cities"][$city->name]["objects"] = $objects;
        $counts = get_data_counts($file_data["access_token"], $file_data["url"], $first_minute, $last_minute, $object_ids);
        if (strpos($counts, "response") !== false) {
            $file_data["cities"][$city->name]["counts_{$current_month}"] = json_encode(json_decode($counts)->response);
        }
        $all_month = get_all_counts($file_data["access_token"], $file_data["url"], $first_minute, $last_minute, $object_ids);
        if (strpos($all_month, "response") !== false) {
            $file_data["cities"][$city->name]["all_{$current_month}"] = json_encode(json_decode($all_month)->response);
        }
        $all_city = get_all_counts($file_data["access_token"], $file_data["url"], "1551398400", "4076006400", $object_ids);
        if (strpos($all_city, "response") !== false) {
            $file_data["cities"][$city->name]["all"] = json_encode(json_decode($all_city)->response);
        }
    }
    file_put_contents("/home/d/delmir01/O2O/public_html/site/templates/files/cron_data.json", json_encode($file_data));
//    file_put_contents("/var/www/html/o2o/site/templates/files/cron_data.json", json_encode($file_data));

}
