<?php

function auth($login, $password, $url)
{
    $data = Array();
    $data["login"] = $login;
    $data["password"] = $password;
    $options = array(
        'http' => array(
            'header' => "Content-type: application/json\r\n",
            'method' => 'POST',
            'content' => json_encode($data)
        )
    );
    $context = stream_context_create($options);
    $result = json_decode(file_get_contents($url . "auth/login", false, $context));
    if ($result !== FALSE) { /* Handle error */
        if ($result->status == "ok") {
            return $result->response;
        }
    }
    return null;
}

function refresh($refresh_token, $url)
{
    $data = Array();
    $data["refresh_token"] = $refresh_token;
    $options = array(
        'http' => array(
            'header' => "Content-type: application/json\r\n",
            'method' => 'POST',
            'content' => json_encode($data)
        )
    );
    $context = stream_context_create($options);
    $result = json_decode(file_get_contents($url . "auth/refresh-token", false, $context));
    if ($result !== FALSE) { /* Handle error */
        if ($result->status == "ok") {
            return $result->response;
        }
    }
    return null;

}

function get_data_counts($access_token, $url, $start, $end, $object_ids)
{
    $headers = array(
        'Content-Type: application/json',
        sprintf('Authorization: Bearer %s', $access_token)
    );
    $curl = curl_init($url . "objects/stats/get?object_ids=" . join(',', $object_ids) . "&start={$start}&end={$end}&unique=true");

    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($curl);
    return $result;
}

function get_all_counts($access_token, $url, $start, $end, $object_ids)
{
    $headers = array(
        'Content-Type: application/json',
        sprintf('Authorization: Bearer %s', $access_token)
    );
    $curl = curl_init($url . "objects/stats/all?object_ids=" . join(',', $object_ids) . "&start={$start}&end={$end}&unique=true");

    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($curl);
    return $result;
}

function get_objects($access_token, $url, $city_id = 0)
{
    $headers = array(
        'Content-Type: application/json',
        sprintf('Authorization: Bearer %s', $access_token)
    );

    $curl = curl_init($url . "objects/get" . (($city_id != 0) ? "?city_id={$city_id}" : ""));

    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($curl);
    return $result;
}

function get_cities($access_token, $url)
{
    $headers = array(
        'Content-Type: application/json',
        sprintf('Authorization: Bearer %s', $access_token)
    );
    $curl = curl_init($url . "cities/get");
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($curl);
    return $result;
}

function get_object_types($access_token, $url)
{
    $headers = array(
        'Content-Type: application/json',
        sprintf('Authorization: Bearer %s', $access_token)
    );
    $curl = curl_init($url . "objects/types/get");
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($curl);
    return $result;
}

function get_tmp_token($login, $access_token, $url)
{
    $data = Array();
    $data["login"] = $login;
    $headers = array(
        'Content-Type: application/json',
        sprintf('Authorization: Bearer %s', $access_token)
    );
    $options = array(
        CURLOPT_RETURNTRANSFER => true,         // return web page
        CURLOPT_HEADER => false,        // don't return headers
        CURLOPT_FOLLOWLOCATION => true,         // follow redirects
        CURLOPT_ENCODING => "",           // handle all encodings
        CURLOPT_CONNECTTIMEOUT => 120,          // timeout on connect
        CURLOPT_TIMEOUT => 120,          // timeout on response
        CURLOPT_MAXREDIRS => 10,           // stop after 10 redirects
        CURLOPT_POST => 1,            // i am sending post data
        CURLOPT_POSTFIELDS => json_encode($data),    // this are my post vars
        CURLOPT_HTTPHEADER => $headers,
    );

    $ch = curl_init($url . "auth/tmp-token");

    curl_setopt_array($ch, $options);
    $res = curl_exec($ch);
    $res_decode = json_decode($res);
    $res_decode->code = (curl_getinfo($ch))["http_code"];
    if ($res_decode->code != 200) {
        $res_decode->response = $res;
    }
    curl_close($ch);
    return $res_decode;
}

function create_order($brief_data, $sum, $payment_requisites_id, $access_token, $url, $coupon_name="")
{
    $data = Array();
    $data["payment_requisite_id"] = $payment_requisites_id;
    $data["brief_data"] = json_decode($brief_data);
    $data["sum"] = floatval($sum);

    if ($coupon_name !== "") {
        $data["coupon_name"] = $coupon_name;
    }

    $headers = array(
        'Content-Type: application/json',
        sprintf('Authorization: Bearer %s', $access_token)
    );
    $options = array(
        CURLOPT_RETURNTRANSFER => true,         // return web page
        CURLOPT_HEADER => false,        // don't return headers
        CURLOPT_FOLLOWLOCATION => true,         // follow redirects
        CURLOPT_ENCODING => "",           // handle all encodings
        CURLOPT_CONNECTTIMEOUT => 120,          // timeout on connect
        CURLOPT_TIMEOUT => 120,          // timeout on response
        CURLOPT_MAXREDIRS => 10,           // stop after 10 redirects
        CURLOPT_POST => 1,            // i am sending post data
        CURLOPT_POSTFIELDS => json_encode($data),    // this are my post vars
        CURLOPT_HTTPHEADER => $headers,
    );

    $ch = curl_init($url . "adv-orders/create");

    curl_setopt_array($ch, $options);
    $res = curl_exec($ch);
    $res_decode = json_decode($res);
    $res_decode->code = (curl_getinfo($ch))["http_code"];
    if ($res_decode->code != 200) {
        $res_decode->response = $res;
    }
    curl_close($ch);
    return $res_decode;
}

function create_payments_requisites($name, $kpp, $address, $inn, $access_token, $url)
{
    $data = Array();
    $data["name"] = $name;
    $data["kpp"] = $kpp;
    $data["address"] = $address;
    $data["inn"] = $inn;
    $headers = array(
        'Content-Type: application/json',
        sprintf('Authorization: Bearer %s', $access_token)
    );
    $options = array(
        CURLOPT_RETURNTRANSFER => true,         // return web page
        CURLOPT_HEADER => false,        // don't return headers
        CURLOPT_FOLLOWLOCATION => true,         // follow redirects
        CURLOPT_ENCODING => "",           // handle all encodings
        CURLOPT_CONNECTTIMEOUT => 120,          // timeout on connect
        CURLOPT_TIMEOUT => 120,          // timeout on response
        CURLOPT_MAXREDIRS => 10,           // stop after 10 redirects
        CURLOPT_POST => 1,            // i am sending post data
        CURLOPT_POSTFIELDS => json_encode($data),    // this are my post vars
        CURLOPT_HTTPHEADER => $headers,
    );

    $ch = curl_init($url . "payment-requisites/create");

    curl_setopt_array($ch, $options);
    $res = curl_exec($ch);
    $res_decode = json_decode($res);
    $res_decode->code = (curl_getinfo($ch))["http_code"];
    if ($res_decode->code != 200) {
        $res_decode->response = $res;
    }
    curl_close($ch);
    return $res_decode;
}

function get_invoice($id, $access_token, $url)
{
    $headers = array(
        'Content-Type: application/json',
        sprintf('Authorization: Bearer %s', $access_token)
    );
    $curl = curl_init($url . "invoices/generate-html?invoice_id={$id}");
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $result = json_decode(curl_exec($curl));
    if ($result !== FALSE) {
        if ($result->status == "ok") {
            return $result->response;
        }
    }
    return null;
}

function get_owner_info($object_id, $access_token, $url)
{
    $headers = array(
        'Content-Type: application/json',
        sprintf('Authorization: Bearer %s', $access_token)
    );
    $curl = curl_init($url . "objects/owner-info/get?object_id={$object_id}");
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $res = curl_exec($curl);
    $res_decode = json_decode($res);
    $res_decode->code = (curl_getinfo($curl))["http_code"];
    if ($res_decode->code != 200) {
        $res_decode->response = $res;
    }
    curl_close($curl);
    return $res_decode;
}

function registr_user($login, $access_token, $url)
{
    $data = Array();
    $data["login"] = $login;
    $data["generate_password"] = true;

    $headers = array(
        'Content-Type: application/json',
        sprintf('Authorization: Bearer %s', $access_token)
    );
    $options = array(
        CURLOPT_RETURNTRANSFER => true,         // return web page
        CURLOPT_HEADER => false,        // don't return headers
        CURLOPT_FOLLOWLOCATION => true,         // follow redirects
        CURLOPT_ENCODING => "",           // handle all encodings
        CURLOPT_CONNECTTIMEOUT => 120,          // timeout on connect
        CURLOPT_TIMEOUT => 120,          // timeout on response
        CURLOPT_MAXREDIRS => 10,           // stop after 10 redirects
        CURLOPT_POST => 1,            // i am sending post data
        CURLOPT_POSTFIELDS => json_encode($data),    // this are my post vars
        CURLOPT_HTTPHEADER => $headers,
    );

    $ch = curl_init($url . "users/registration");

    curl_setopt_array($ch, $options);
    $res = curl_exec($ch);
    $res_decode = json_decode($res);
    $res_decode->code = (curl_getinfo($ch))["http_code"];
    if ($res_decode->code != 200) {
        $res_decode->response = $res;
    }
    curl_close($ch);
    return $res_decode;
}

function get_coupon_by_name($coupon_name, $access_token, $url)
{
    $headers = array(
        'Content-Type: application/json',
        sprintf('Authorization: Bearer %s', $access_token)
    );
    $curl = curl_init($url . "coupons/get?name={$coupon_name}");
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $res = curl_exec($curl);
    $res_decode = json_decode($res);
    $res_decode->code = (curl_getinfo($curl))["http_code"];
    if ($res_decode->code != 200) {
        $res_decode->response = $res;
    }
    curl_close($curl);
    return $res_decode;
}