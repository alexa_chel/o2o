const cities_obj = {
    'Тюмень': {
        coords: [57.15303, 65.53432],
        abbreviation: 'tmn'
    },
    'Москва': {
        coords: [55.753215, 37.622504],
        abbreviation: 'msk'
    },
    'Ульяновск': {
        coords: [54.314192, 48.403123],
        abbreviation: 'uly'
    },
    'Ярославль': {
        coords: [57.626578, 39.893858],
        abbreviation: 'yar'
    },
    'Томск': {
        coords: [56.484640, 84.947649],
        abbreviation: 'tom'
    },
    'Тольятти': {
        coords: [53.508816, 49.419207],
        abbreviation: 'tlt'
    },
    'Оренбург': {
        coords: [51.768199, 55.096955],
        abbreviation: 'orb'
    },
    'Кемерово': {
        coords: [55.354727, 86.088374],
        abbreviation: 'kem'
    },
    'Казань': {
        coords: [55.796289, 49.108795],
        abbreviation: 'kzn'
    },
    'Санкт-Петербург': {
        coords: [59.939095, 30.315868],
        abbreviation: 'spb'
    },
    'Нижний Новгород': {
        coords: [56.326887, 44.005986],
        abbreviation: 'nnov'
    },
    'Челябинск': {
        coords: [55.159897, 61.402554],
        abbreviation: 'chl'
    },
    'Новокузнецк': {
        coords: [53.757547, 87.136044],
        abbreviation: 'nkz'
    },
    'Рязань': {
        coords: [54.629216, 39.736375],
        abbreviation: 'ryz'
    },
    'Астрахань': {
        coords: [46.347869, 48.033574],
        abbreviation: 'ast'
    },
    'Набережные Челны': {
        coords: [55.740776, 52.406384],
        abbreviation: 'nch'
    },
    'Краснодар': {
        coords: [45.035470, 38.975313],
        abbreviation: 'krd'
    },
    'Красноярск': {
        coords: [56.010563, 92.852572],
        abbreviation: 'kry'
    },
    'Уфа': {
        coords: [54.735147, 55.958727],
        abbreviation: 'ufa'
    },
    'Самара': {
        coords: [53.195538, 50.101783],
        abbreviation: 'sam'
    },
    'Волгоград': {
        coords: [48.707073, 44.516930],
        abbreviation: 'vgg'
    },
    'Воронеж': {
        coords: [51.660781, 39.200269],
        abbreviation: 'vrn'
    },
    'Киров': {
        coords: [58.603591, 49.668014],
        abbreviation: 'kir'
    },
    'Пенза': {
        coords: [53.195063, 45.018316],
        abbreviation: 'pnz'
    },
    'Липецк': {
        coords: [52.608820, 39.599220],
        abbreviation: 'lip'
    },
    'Иваново': {
        coords: [57.000348, 40.973921],
        abbreviation: 'iva'
    },
    'Брянск': {
        coords: [53.243562, 34.363407],
        abbreviation: 'bry'
    },
    'Белгород': {
        coords: [50.595660, 36.587223],
        abbreviation: 'bel'
    },
    'Мурманск': {
        coords: [68.970682, 33.074981],
        abbreviation: 'mur'
    },
    'Калининград': {
        coords: [54.707390, 20.507307],
        abbreviation: 'klg'
    },
    'Севастополь': {
        coords: [44.616604, 33.525369],
        abbreviation: 'svs'
    },
    'Пермь': {
        coords: [58.010374, 56.229398],
        abbreviation: 'prm'
    },
    'Екатеринбург': {
        coords: [56.838011, 60.597465],
        abbreviation: 'ekb'
    },
    'Омск': {
        coords: [54.989342, 73.368212],
        abbreviation: 'oms'
    },
    'Саратов': {
        coords: [51.533103, 46.034158],
        abbreviation: 'sar'
    },
    'Ростов-на-Дону': {
        coords: [47.222078, 39.720349],
        abbreviation: 'rnd'
    },
    'Ставрополь': {
        coords: [45.044521, 41.969083],
        abbreviation: 'sta'
    },
    'Сочи': {
        coords: [43.585525, 39.723062],
        abbreviation: 'sch'
    },
    'Тверь': {
        coords: [56.859847, 35.911995],
        abbreviation: 'tve'
    },
    'Чебоксары': {
        coords: [56.146277, 47.251079],
        abbreviation: 'chb'
    },
    'Тула': {
        coords: [54.193122, 37.617348],
        abbreviation: 'yul'
    },
    'Курск': {
        coords: [51.730361, 36.192647],
        abbreviation: 'kur'
    },
    'Архангельск': {
        coords: [64.539911, 40.515753],
        abbreviation: 'arh'
    },
    'Владимир': {
        coords: [56.129057, 40.406635],
        abbreviation: 'vla'
    },
    'Ижевск': {
        coords: [56.852593, 53.204843],
        abbreviation: 'izh'
    },
    'Сургут': {
        coords: [61.254035, 73.396221],
        abbreviation: 'sur'
    },
    'Курган': {
        coords: [55.441004, 65.341118],
        abbreviation: 'krg'
    }
};
const dative_months_obj = {
    1: 'январе',
    2: 'феврале',
    3: 'марте',
    4: 'апреле',
    5: 'мае',
    6: 'июне',
    7: 'июле',
    8: 'августе',
    9: 'сентябре',
    10: 'октябре',
    11: 'ноябре',
    12: 'декабре',
};
const months_obj = {
    1: 'январь',
    2: 'февраль',
    3: 'март',
    4: 'апрель',
    5: 'май',
    6: 'июнь',
    7: 'июль',
    8: 'август',
    9: 'сентябрь',
    10: 'октябрь',
    11: 'ноябрь',
    12: 'декабрь',
};
const outdoors_type_ids = [1, 2, 3, 7, 12, 13, 16, 17, 20, 23, 24];
let apply_coupon_val = null;
const sections_name = {
    outdoors: 'Реклама на outdoor-конструкциях',
    macs: 'Данные, собранные системой О2О',
    platforms: 'Рекламные площадки',
    ta: 'Целевая аудитория',
    site: 'Информация о сайте',
    banners: 'Дизайн РК',
    shows: 'Длительность и количество показов РК',
    smm: 'SMM',
    serm: 'Репутация',
};
const fields_name = {
    outdoors: {
        outdoors_object_ids: 'объекты'
    },
    macs: {
        macs_object_ids: 'объекты',
        macs_count: 'кол-во маков',
        start: 'с',
        end: 'по'
    },
    platforms: {
        platforms_auto: 'довериться специалистам О2О',
        platforms_yandex: 'Yandex',
        platforms_my_target: 'MyTarget',
        platforms_facebook: 'Facebook',
        platforms_google: 'Google',
        platforms_youtube: 'Youtube'
    },
    ta: {
        ta_auto: 'довериться специалистам О2О',
        ta_gender: 'пол',
        ta_age_from: 'возраст от',
        ta_age_to: 'возраст до',
        ta_revenue_from: 'доход от',
        ta_revenue_to: 'доход до',
        ta_locations: 'геолокация',
        ta_additional_info: 'доп-ые критерии'
    },
    site: {
        site_need: 'есть ли сайт',
        site_type: 'вид сайта',
        site_count_page: 'кол-во страниц',
        site_need_help: 'установлена ли система аналитики',
        site_google: 'Google Analytics',
        site_yandex: 'Yandex.Метрика',
        site_targets: 'цели в метрике',
        site_call_tracking: 'коллтрекинг'
    },
    banners: {
        banners_creative_company: 'создание уникальной креативной компании',
        banners_exist: 'есть ли баннеры',
        banners_need_adaptation: 'адаптация баннеров',
        banners_template: 'шаблонный ли макет',
        banners_creatives_count: 'кол-во креативов',
        banners_video: 'создание видео'
    },
    shows: {
        shows_count: 'кол-во показов',
        shows_runtime_weeks: 'длительность (недель)'
    },
    smm: {
        smm_create_vk: 'создание VK',
        smm_create_instagram: 'создание Instagram',
        smm_create_facebook: 'создание Facebook',
        smm_management_vk: 'ведение VK',
        smm_management_instagram: 'ведение Instagram',
        smm_management_facebook: 'ведение Facebook',
        smm_create_content: 'нужна ли помощь с созданием контента',
        smm_decor_vk: 'оформление VK',
        smm_decor_instagram: 'оформление Instagram',
        smm_decor_facebook: 'оформление Facebook',
    },
    serm: {
        serm_reputation: 'устранение негатива, размещение отзывов, создание положительного инфополя',
        serm_feedbacks: 'ответы на отзывы от лица бренда'
    }
};
let brief_data = {
    outdoors_object_ids: [],
    macs_object_ids: [],
    macs_count: 0,
    start: null,
    end: null,
    platforms_auto: false,
    platforms_yandex: false,
    platforms_my_target: false,
    platforms_facebook: false,
    platforms_google: false,
    platforms_youtube: false,
    ta_auto: false,
    ta_gender: 2,
    ta_age_from: 21,
    ta_age_to: 54,
    ta_revenue_from: 35000,
    ta_revenue_to: 150000,
    ta_locations: [],
    ta_additional_info: '',
    site_need: true,
    site_type: 0,
    site_count_page: 1,
    site_need_help: true,
    site_google: false,
    site_yandex: false,
    site_targets: false,
    site_call_tracking: false,
    banners_creative_company: false,
    banners_exist: false,
    banners_need_adaptation: false,
    banners_template: false,
    banners_creatives_count: 1,
    banners_video: false,
    shows_count: 10000,
    shows_runtime_weeks: 1,
    smm_create_vk: false,
    smm_create_instagram: false,
    smm_create_facebook: false,
    smm_management_vk: false,
    smm_management_instagram: false,
    smm_management_facebook: false,
    smm_create_content: false,
    smm_decor_vk: false,
    smm_decor_instagram: false,
    smm_decor_facebook: false,
    serm_reputation: false,
    serm_feedbacks: false
};
let brief_data_storage = sessionStorage.getItem('o2o_brief_data');

try {
    brief_data_storage = JSON.parse(brief_data_storage);
} catch (e) {
    brief_data_storage = null
}

if (brief_data_storage) {
    brief_data = brief_data_storage;
}

$(function () {
    const $page = $('.page');
    const $selectCity = $('#select-map-city select');
    const $selectTypeObject = $('#select-map-type-object select');
    const $selectCityLabel = $('#select-map-city .select-custom__option span');
    const $selectTypeObjectLabel = $('#select-map-type-object .select-custom__option span');
    const $briefOrder = $('.brief-order');
    const $finalSum = $("#final-price span");
    const $finalSumOld = $(".old-sum");
    const $slider_age = $("#slider-age");
    const $slider_income = $("#slider-income");
    const $age_from = $("#custom-handle-1");
    const $age_to = $("#custom-handle-2");
    const $revenue_from = $("#custom-handle-3");
    const $revenue_to = $("#custom-handle-4");
    const $owlCarouselCalc = $(".owl-carousel-calc");
    const $action = $('.action');
    const $briefs = $('.briefs');
    const $briefResult = $('.brief-result');
    const $btnRemoveAll = $('.btn-remove-all');
    const $advPlatformTrust = $('#adv-platform-trust');
    const $platforms_yandex = $('[name="adv-platform"][data-answer="yandex"]');
    const $platforms_my_target = $('[name="adv-platform"][data-answer="mytarget"]');
    const $platforms_facebook = $('[name="adv-platform"][data-answer="facebook"]');
    const $platforms_google = $('[name="adv-platform"][data-answer="google"]');
    const $platforms_youtube = $('[name="adv-platform"][data-answer="youtube"]');
    const $male = $('[name="gender"][data-answer="male"]');
    const $female = $('[name="gender"][data-answer="female"]');
    const $ta = $('.ca-trust');
    const $ta_auto = $('[name="ca-trust"][data-answer="yes"]');
    const $ta_additional_info = $('[name="additional-info"]');
    const $btn_back = $('[data-direction="prev"]');
    const $btn_next = $('[data-direction="next"]');
    const $createCompany = $('[name="create-company"]');
    const $createVideo = $('[name="create-video"]');
    const $blockSocial = $('.block-social');
    const $blockSocialCreate = $blockSocial.find('[name="create"]');
    const $blockSocialVedenie= $blockSocial.find('[name="vedenie"]');
    const $blockSocialDecor= $blockSocial.find('[name="decor"]');
    const $blockSocialVk = $('.block-social[data-name="vk"]');
    const $blockSocialVkCreate = $blockSocialVk.find('[name="create"]');
    const $blockSocialVkVedenie = $blockSocialVk.find('[name="vedenie"]');
    const $blockSocialVkDecor = $blockSocialVk.find('[name="decor"]');
    const $blockSocialInst = $('.block-social[data-name="inst"]');
    const $blockSocialInstCreate = $blockSocialInst.find('[name="create"]');
    const $blockSocialInstVedenie = $blockSocialInst.find('[name="vedenie"]');
    const $blockSocialInstDecor = $blockSocialInst.find('[name="decor"]');
    const $blockSocialFacebook = $('.block-social[data-name="facebook"]');
    const $blockSocialFacebookCreate = $blockSocialFacebook.find('[name="create"]');
    const $blockSocialFacebookVedenie = $blockSocialFacebook.find('[name="vedenie"]');
    const $blockSocialFacebookDecor = $blockSocialFacebook.find('[name="decor"]');
    const $supportContent = $('[name="support-content"]');
    const $supportContentYes = $('[name="support-content"][data-answer="yes"]');
    const $supportContentNo = $('[name="support-content"][data-answer="no"]');
    const $blockReputation = $('.block-reputation');
    const $blockReputationInputs = $blockReputation.find('input[type="checkbox"]');
    const $blockReputationNegative = $blockReputation.find('[data-answer="negative"]');
    const $blockReputationReviews = $blockReputation.find('[data-answer="reviews"]');
    const $systemAnalytics = $('[name="system-analytics"]');
    const $systemAnalyticsNo = $('[name="system-analytics"][data-answer="no"]');
    const $systemAnalyticsYes = $('[name="system-analytics"][data-answer="yes"]');
    const $blockDesign = $('.block-design');
    const $siteGoogle =  $('[name="ga"]');
    const $siteYandex =  $('[name="yam"]');
    const $siteTargets =  $('[name="targets"]');
    const $siteTargets2 =  $('[data-field="site_targets"]');
    const $siteCallTracking =  $('[name="calltracking"]');
    const $siteGoogleText = $siteGoogle.closest('.cont').find('.word');
    const $siteYandexText = $siteYandex.closest('.cont').find('.word');
    const $siteTargetsText = $siteTargets.closest('.cont').find('.word');
    const $siteCallTrackingText = $siteCallTracking.closest('.cont').find('.word');
    const $viewPage = $('.view-page [name="targets"]');
    const $priceSite = $('[name="price-site"]');
    const $priceSiteBlock = $('.price-site');
    const $typeSite = $('.type-site');
    const $singlePage = $('.view-page [name="targets"][data-answer="single-page"]');
    const $multiPage = $('.view-page [name="targets"][data-answer="multi-page"]');
    const $hasSite = $('[name="has-site"]');
    const $hasSiteNo = $('[name="has-site"][data-answer="no"]');
    const $hasSiteYes = $('[name="has-site"][data-answer="yes"]');
    const $selectCountCreatives = $('.count-creatives select');
    const $selectCountLayouts = $('.count-layouts select');
    const $hasCreativesLayouts = $('.has-creatives [name="layouts"]');
    const $layoutsYesNo = $('[name="layouts"][data-answer="yes-no"]');
    const $layoutsNo = $('[name="layouts"][data-answer="no"]');
    const $layoutsYes = $('[name="layouts"][data-answer="yes"]');
    const $countLayouts = $('.count-layouts');
    const $supportDesigner = $('.support-designer');
    const $support = $('.support-designer [name="support"]');
    const $countCreatives = $('.support-designer .count-creatives');
    const $interval = $('[name="interval"]');
    const $selectCountShows = $('.count-shows select');
    const $selectCitiesModal = $('.select-cities');
    const $supportTemplate = $('[name="support"][data-answer="template"]');
    const $period = $('.map-period input');
    const city = {value: cities_obj['Тюмень'].coords, label: 'Тюмень'};
    const start_default = moment().startOf('month').subtract(1, 'months').toDate();
    const end_default = moment().endOf('month').subtract(1, 'months').toDate();
    let showAllMacs = true,
        oldAllMacs = null,
        types_obj = {},
        objects = [],
        cities = {},
        radars_obj = {},
        currentStep = 1,
        map, map2, myGroup, city_name,
        myGroup2, objects_data;

    if (!$page.hasClass('page-calc')) {
        new WOW().init();

        setTimeout(() => $('.page-owner__we-do li:nth-child(1)').addClass('go'), 300);
        setTimeout(() => $('.page-owner__we-do li:nth-child(2)').addClass('go'), 500);
        setTimeout(() => $('.page-owner__we-do li:nth-child(3)').addClass('go'), 700);
        setTimeout(() => $('.page-owner__we-do li:nth-child(4)').addClass('go'), 900);
        setTimeout(initPage, 2000);
    } else {
        initPage()
    }

    function initPage() {
        if (!$page.hasClass('page-calc')) {
            $('.strikethrough-wrapper1 .strikethrough').viewportChecker({
                classToAdd: 'go',
                offset: 100
            });

            $('.strikethrough-wrapper2 .strikethrough').viewportChecker({
                classToAdd: 'go',
                offset: 100
            });

            $('.strikethrough-wrapper3 .strikethrough').viewportChecker({
                classToAdd: 'go',
                offset: 100
            });

            $('.strikethrough-wrapper4 .strikethrough').viewportChecker({
                classToAdd: 'go',
                offset: 100
            });

            $('.strikethrough-wrapper5 .strikethrough').viewportChecker({
                classToAdd: 'go',
                offset: 100
            });
        }

        if ($page.hasClass('page-main')) {
            const page = 'main';

            $('#way').viewportChecker({
                offset: 100,
                callbackFunction: function() {
                    const paths = $('#way > g:nth-child(2) > g:first-child path');
                    let time = 0;

                    paths.each(function (i, elem) {
                        setTimeout(() => $(elem).addClass('go'), time);
                        time += 30;
                    });
                }
            });

            $('#way-mobile').viewportChecker({
                offset: 100,
                callbackFunction: function() {
                    const paths = $('#way-mobile > g:nth-child(2) > path');
                    let time = 0;

                    paths.each(function (i, elem) {
                        setTimeout(() => $(elem).addClass('go'), time);
                        time += 30;
                    });
                }
            });

            $(".owl-carousel-cases").owlCarousel({
                loop: true,
                margin: 10,
                nav: true,
                items: 1,
                dots: true,
                autoplay: true,
                autoplayTimeout: 10000,
                autoplayHoverPause: true,
                navText : ["<div class='carousel-arrow-left'></div>","<div class='carousel-arrow-right'></div>"]
            });

            ymaps.ready(function () {
                const center = [61.698653, 99.505405];
                const controls = ['zoomControl', 'typeSelector', 'fullscreenControl'];
                const zoom = 4;

                map = new ymaps.Map("ymaps", { center, controls, zoom });
                map.behaviors.disable('scrollZoom');

                getCounts(page)
            });

            setInterval(function () {
                getCounts(page, false)
            }, 60000);
        } else if ($page.hasClass('page-documents')) {
            $('.read').click(function(e) {
                e.preventDefault();
                window.open($(this).data('href'));
                window.location.href = window.location.origin + $(this).data('href');
            });
        } else if ($page.hasClass('page-map')) {
            const page = 'map';
            city_name = $('.section-city-map').attr('data-city');

            $('.owl-carousel-months').owlCarousel({
                items: $(window).width() <= 640 ? 4 : 10,
                nav: true,
                navText: ["<img src='/site/templates/img/arrow.svg' alt=''>", "<img src='/site/templates/img/arrow.svg' alt=''>"]
            });

            disabledMonths();

            function disabledMonths() {
                const d = new Date();
                const month = d.getMonth() + 1;
                const $target_month = $(`#month_${month}`);

                for (let i = month + 1; i <= 12; i++) {
                    $(`#month_${i}`).closest('.owl-item').addClass('disabled')
                }

                $target_month.addClass('active');
                $target_month.append('<div class="strikethrough go"></div>')
            }

            $('#months').css('display', 'block');

            let start_date = moment().startOf('month').unix(), end_date = moment().unix();
            ymaps.ready(function () {
                const center = cities_obj[city_name].coords;
                const controls = ['zoomControl', 'typeSelector', 'fullscreenControl'];
                const zoom = 13;

                map = new ymaps.Map("city-map", { center, controls, zoom });
                map.behaviors.disable('scrollZoom');

                getCounts(page)
            });

            $('.hint span').text(dative_months_obj[$('.owl-item .active').attr('data-number')]);

            $('#months .owl-item').click(function () {
                $(`#months .owl-item .active`).removeClass('active');
                $('#months .owl-item').find('.strikethrough').remove();
                $('.map-wrapper').removeClass('hidden');

                const $month = $(this).find('div');
                $month.append('<div class="strikethrough go"></div>');

                $(this).find("> div").addClass('active');

                const d = new Date();
                const month = d.getMonth() + 1;

                if (Number($month.attr('data-number')) === month) {
                    start_date = moment().startOf('month').unix();
                    end_date = moment().unix();
                } else {
                    start_date = moment().add(-(month - Number($month.attr('data-number'))), 'months').startOf('month').unix();
                    end_date = moment().add(-(month - Number($month.attr('data-number'))), 'months').endOf('month').unix()
                }

                $('.hint span').text(dative_months_obj[$month.attr('data-number')]);

                setMapData(page)
            });

            setInterval(function () {
                getCounts(page, false)
            }, 60000);
        } else if ($page.hasClass('page-calc')) {
            city_name = city.label;

            initCalc();
            setInterval(initCalc, 5000);

            $('.section-brief').css('opacity', 1);

            ymaps.ready(function () {
                const center = cities_obj[city_name].coords;
                const controls = ['zoomControl', 'typeSelector', 'fullscreenControl', 'searchControl'];
                const zoom = 13;

                map = new ymaps.Map("map", { center, controls, zoom });
                map2 = new ymaps.Map("map2", { center, controls, zoom });

                getCounts('calc')
            });

            function onChangeSelects() {
                $selectTypeObject.change(function (e) {
                    const value = Number(e.target.value);
                    $selectTypeObjectLabel.text($($selectTypeObject.find(':selected')[0]).text());

                    myGroup2.each(function (el) {
                        el.options.set('visible', value !== -1 ?
                            (Number(el.properties.get('object_type_id')) === value) : true)
                    });
                });

                $selectCity.change(function (e) {
                    const coords = e.target.value.split(',');
                    const text = $(this).find(':selected').text();

                    map && map.setCenter(coords);
                    map2 && map2.setCenter(coords);

                    $($selectCityLabel[0]).text(text);
                    $($selectCityLabel[1]).text(text);

                    city_name = text;
                    setMapData('calc')
                });
            }

            $btnRemoveAll.click(function () {
                [...(currentStep === 1 ? brief_data.outdoors_object_ids : brief_data.macs_object_ids)].map(id => {
                    onClickPlacemark(true, id, true)
                })
            });

            $countLayouts.hide();

            [$advPlatformTrust, $createCompany, $createVideo, $blockSocialCreate, $blockSocialVedenie, $blockSocialDecor,
                $supportContent, $blockReputationInputs, $platforms_yandex, $platforms_my_target, $platforms_facebook,
                $platforms_google, $platforms_youtube, $systemAnalytics, $viewPage, $priceSite, $hasSite, $siteTargets2,
                $ta, $selectCountCreatives, $selectCountLayouts, $hasCreativesLayouts, $support, $interval,
                $selectCountShows, $ta_additional_info, $siteGoogle, $siteYandex, $siteCallTracking,
                $siteTargets, $male, $female].map(element => {
                element.change(function (e) {
                    const field = $(this).attr('data-field');
                    const answer = $(this).attr('data-answer');

                    setBriefDataField($(this), field, answer);
                    changeOrder();
                })
            });

            function setBriefDataField(_this, field, answer) {
                switch (field) {
                    case 'platforms_auto':
                        onChangePlatform(true);
                        break;
                    case 'platforms_yandex':
                        onChangePlatform();
                        break;
                    case 'platforms_my_target':
                        onChangePlatform();
                        break;
                    case 'platforms_facebook':
                        onChangePlatform();
                        break;
                    case 'platforms_google':
                        onChangePlatform();
                        break;
                    case 'platforms_youtube':
                        onChangePlatform();
                        break;
                    case 'banners_creative_company':
                        if (_this.prop('checked'))
                            $blockDesign.addClass('disabled');
                        else
                            $blockDesign.removeClass('disabled');

                        brief_data.banners_creative_company = $createCompany.prop('checked');

                        if (!$createCompany.prop('checked')) {
                            // setBriefDataField('banners_need_adaptation', $(this).attr('data-answer'))
                            brief_data.banners_exist = $layoutsYes.prop('checked');
                            brief_data.banners_need_adaptation = $layoutsYesNo.prop('checked');
                            brief_data.banners_template = $supportTemplate.prop('checked');
                            brief_data.banners_video = $createVideo.prop('checked');

                            setBannerCreativesCount();
                        }
                        break;
                    case 'site_need_help':
                        if (answer === 'yes') {
                            $siteGoogleText.text('Подключен GoogleAnalytics');
                            $siteYandexText.text('Подключена Яндекс Метрика');
                            $siteTargetsText.text('Раставлены цели в метрике');
                            $siteCallTrackingText.text('Подключен коллтрекинг');
                        } else {
                            $siteGoogleText.text('Подключить GoogleAnalytics');
                            $siteYandexText.text('Подключить Яндекс Метрику');
                            $siteTargetsText.text('Раставить цели в метрике');
                            $siteCallTrackingText.text('Подключить коллтрекинг');
                        }

                        brief_data.site_need_help = $systemAnalyticsNo.prop('checked');
                        break;
                    case 'site_type':
                        if (Number(answer) === 50000)
                            $singlePage.prop('checked', true);
                        else
                            $multiPage.prop('checked', true);

                        brief_data.site_type = $('[name="price-site"][data-answer="50000"]').prop('checked') ? 1 :
                            $('[name="price-site"][data-answer="100000"]').prop('checked') ? 2 :
                                $('[name="price-site"][data-answer="300000"]').prop('checked') ? 3 : 0;
                        break;
                    case 'site_need':
                        if (answer === 'no') {
                            $priceSiteBlock.show();
                            $typeSite.addClass('disabled');
                            $systemAnalyticsYes.closest('.cont').addClass('disabled');
                        } else {
                            $priceSiteBlock.hide();
                            $typeSite.removeClass('disabled');
                            $systemAnalyticsYes.closest('.cont').removeClass('disabled');
                        }

                        brief_data.site_need = $hasSiteNo.prop('checked');
                        break;
                    case 'site_google':
                        brief_data.site_google = $siteGoogle.prop('checked');
                        break;
                    case 'site_yandex':
                        brief_data.site_yandex = $siteYandex.prop('checked');
                        break;
                    case 'site_targets':
                        brief_data.site_targets = $siteTargets2.prop('checked');
                        break;
                    case 'site_call_tracking':
                        brief_data.site_call_tracking = $siteCallTracking.prop('checked');
                        break;
                    case 'ca_trust':
                        const $blockHide = $('.brief[data-step="4"] [data-hide]');

                        if (answer === 'yes')
                            $blockHide.addClass('disabled');
                        else
                            $blockHide.removeClass('disabled');

                        brief_data.ta_auto = $ta_auto.prop('checked');
                        break;
                    case 'ta_gender':
                        const male_val = $male.prop('checked');
                        const female_val = $female.prop('checked');
                        let gender = 2;

                        if (male_val && !female_val)
                            gender = 1;
                        else if (!male_val && female_val)
                            gender = 0;

                        brief_data.ta_gender = gender;
                        break;
                    case 'select':
                        _this.closest('.select-custom')
                            .find('.select-custom__option span')
                            .text($(_this.find(':selected')[0]).text());

                        brief_data.shows_count = Number($('.count-shows :selected').val());

                        setBannerCreativesCount();
                        break;
                    case 'banners_need_adaptation':
                        if (answer === 'yes' || answer === 'yes-no')
                            $supportDesigner.addClass('disabled');
                        else if (answer === 'no')
                            $supportDesigner.removeClass('disabled');

                        if (answer === 'yes-no')
                            $countLayouts.show();
                        else if (answer === 'yes' || answer === 'no')
                            $countLayouts.hide();


                        brief_data.banners_exist = $layoutsYes.prop('checked');
                        brief_data.banners_need_adaptation = $layoutsYesNo.prop('checked');
                        setBannerCreativesCount();
                        break;
                    case 'banners_template':
                        if (answer === 'unique')
                            $countCreatives.show();
                        else
                            $countCreatives.hide();

                        brief_data.banners_template = $supportTemplate.prop('checked');
                        setBannerCreativesCount();
                        break;
                    case 'banners_video':
                        brief_data.banners_video = $createVideo.prop('checked');
                        break;
                    case 'ta_additional_info':
                        brief_data.ta_additional_info = $ta_additional_info.val();
                        break;
                    case 'shows_runtime_weeks':
                        let runtime_weeks = 1;
                        const interval = $('[name="interval"]:checked').attr('data-answer');

                        if (interval === '2-week')
                            runtime_weeks = 2;
                        else if (interval === '1-month')
                            runtime_weeks = 4;

                        brief_data.shows_runtime_weeks = runtime_weeks;
                        break;
                    case 'smm':
                        brief_data.smm_create_vk = $blockSocialVkCreate.prop('checked');
                        brief_data.smm_create_instagram = $blockSocialInstCreate.prop('checked');
                        brief_data.smm_create_facebook = $blockSocialFacebookCreate.prop('checked');
                        brief_data.smm_management_vk = $blockSocialVkVedenie.prop('checked');
                        brief_data.smm_management_instagram = $blockSocialInstVedenie.prop('checked');
                        brief_data.smm_management_facebook = $blockSocialFacebookVedenie.prop('checked');
                        brief_data.smm_create_content = $supportContentYes.prop('checked');
                        brief_data.smm_decor_vk = $blockSocialVkDecor.prop('checked');
                        brief_data.smm_decor_instagram = $blockSocialInstDecor.prop('checked');
                        brief_data.smm_decor_facebook = $blockSocialFacebookDecor.prop('checked');
                        break;
                    case 'serm':
                        brief_data.serm_reputation = $blockReputationNegative.prop('checked');
                        brief_data.serm_feedbacks = $blockReputationReviews.prop('checked');
                        break;
                    default:
                        break;
                }

                let count_page = 1;
                if ($multiPage.prop('checked')) {
                    count_page = 2;
                }

                brief_data.site_count_page = count_page;

                sessionStorage.setItem('o2o_brief_data', JSON.stringify(brief_data))
            }

            function setBannerCreativesCount() {
                let count = 1;

                if ($layoutsYesNo.prop('checked')) {
                    count = Number($('.count-layouts :selected').val())
                } else {
                    count = Number($('.count-creatives :selected').val())
                }

                brief_data.banners_creatives_count = count;
            }

            onChangeSelects();

            $(".dialog-invoice").dialog({
                autoOpen: false,
                draggable: false,
                closeText : '',
                beforeClose: function() {
                    $('.modal-wrapper-invoice').css('display', 'none');
                },
                open: function() {
                    $('.modal-wrapper-invoice').css('display', 'block');
                }
            });

            $('.modal-wrapper-invoice').click(function () {
                $(".dialog-invoice").dialog( "close" );
            });

            $(".dialog-params").dialog({
                autoOpen: false,
                draggable: false,
                closeText : '',
                beforeClose: function() {
                    $('.modal-wrapper-params').css('display', 'none');
                },
                open: function() {
                    $('.modal-wrapper-params').css('display', 'block');
                }
            });

            $('.modal-wrapper-params').click(function () {
                $(".dialog-params").dialog( "close" );
            });

            $(".dialog-invoice-html").dialog({
                autoOpen: false,
                draggable: false,
                closeText : '',
                beforeClose: function() {
                    $('.modal-wrapper-invoice-html').css('display', 'none');
                },
                open: function() {
                    $('.modal-wrapper-invoice-html').css('display', 'block');
                }
            });

            $('.modal-wrapper-invoice-html').click(function () {
                $(".dialog-invoice-html").dialog( "close" );
            });

            $('#checkout').click(function () {
                $(".dialog-params").dialog("open");
            });

            // $('#checkout').click(function () {
            //     $(".dialog-invoice").dialog("open");s
            // });

            const $formNotification = $(".dialog-notification");
            let dialog_invoice_open = true;

            $formNotification.dialog({
                autoOpen: false,
                draggable: false,
                closeText : '',
                beforeClose: function() {
                    if (dialog_invoice_open)
                        $('.dialog-invoice').dialog("open");
                    dialog_invoice_open = false;

                    $('.modal-wrapper-notification').css('display', 'none');
                },
                open: function() {
                    $formNotification.closest('.ui-dialog').css('z-index', '1000000002');
                    $('.modal-wrapper-notification').css('display', 'block');
                }
            });

            $formNotification.closest('.ui-dialog').css('max-width', '600px');

            $('.modal-wrapper-notification').click(function () {
                $formNotification.dialog( "close" );
            });

            $action.click(function (e) {
                const attributes = e.target.attributes;
                let target_step = null, direction = null;

                if (attributes.getNamedItem('data-step'))
                    target_step = Number(attributes.getNamedItem('data-step').value);
                else {
                    target_step = Number($(this).closest('[data-step]').attr('data-step'));
                }

                if ($(this).hasClass('brief-step')) {
                    if (target_step > currentStep) {
                        direction = 'next';
                    } else if (target_step < currentStep) {
                        direction = 'prev';
                    }

                    $owlCarouselCalc.trigger("to.owl.carousel", [target_step - 1, 250, true]);
                    onClickStep(target_step, direction, false)
                } else {
                    direction = attributes.getNamedItem('data-direction').value;
                    onClickStep(target_step, direction)
                }
            });

            const $formParams = $('.form-params');
            const $formInvoice = $('.form-invoice');
            const $couponInput = $(".coupon-input");

            $formParams.on("submit", function() {
                event.preventDefault();

                const $btnSubmit = $('.btn-params');
                const params = [];

                $btnSubmit.addClass('disabled');
                $formInvoice.find('[name="login"]').val($formParams.find('[name="login"]').val());

                Object.keys(fields_name).map(section_name => {
                    const services = [];

                    Object.keys(fields_name[section_name]).map(field =>
                        services.push(`${fields_name[section_name][field]}: ${getFormatValue(brief_data[field], field)}`)
                    );

                    params.push(`${sections_name[section_name]} (${services.join(', ')})`)
                });

                $.ajax({
                    type: "POST",
                    url: '/ajax-handler/',
                    data: `${$formParams.serialize()}&message=${params.join(';\n\n')}`,
                    success: () => {
                        $('.dialog-params').dialog("close");
                        dialog_invoice_open = true;
                        $formNotification.find('.modal__caption').html('Спасибо! Ваша заявка принята. В ближайшее время с Вами свяжется наш менеджер');
                        $formNotification.find('.modal__description').html('Теперь Вы можете оставить свои данные для выставления счёта<br>на оплату услуг');
                        $formNotification.dialog("open");

                        $btnSubmit.removeClass('disabled');
                    },
                    error: () => {
                        alert('Произошла ошибка. Повторите попытку позже');
                        $btnSubmit.removeClass('disabled');
                    }
                });
            });

            $formInvoice.on("submit", function() {
                event.preventDefault();

                const coupon_val = $couponInput.val();
                const $btnSubmit = $('.btn-invoiced');

                $btnSubmit.addClass('disabled');

                let data = `${$formInvoice.serialize()}&brief_data=${JSON.stringify(brief_data)}&sum=${getFinalSum()[0]}`;

                if (apply_coupon_val && coupon_val) {
                    data += `&coupon_name=${coupon_val}`
                }

                $.ajax({
                    type: "POST",
                    url: '/ajax-handler/',
                    data,
                    success: response => {
                        response = JSON.parse(response);

                        if (response.status === 'ok') {
                            $('.dialog-invoice').dialog("close");
                            $('.iframe-invoice').contents().find('body').html(response.response);
                            $('.dialog-invoice-html').dialog("open");

                            $formNotification.find('.modal__caption').html('Повторно ознакомиться<br>с выставленным счётом<br>Вы можете в <a href="https://lk.o2o.media" target="_blank">личном кабинете</a>');
                            $formNotification.find('.modal__description').html('Для получения доступа к личному кабинету (при отсутствии аккаунта) Вам необходимо перейти по ссылке из письма, которое было отправлено на указанную почту');
                            $formNotification.dialog("open");
                        } else {
                            alert('Произошла ошибка. Повторите попытку позже')
                        }

                        $btnSubmit.removeClass('disabled');
                    },
                    error: () => {
                        alert('Произошла ошибка. Повторите попытку позже');
                        $btnSubmit.removeClass('disabled');
                    }
                });
            });

            $("#invoice-print").click(function () {
                $(".iframe-invoice").get(0).contentWindow.print();
            });
            
            $("#apply-coupon").click(function () {
                const coupon_val = $couponInput.val();
                const _this = $(this);
                const sum = getFinalSum()[0];

                if (coupon_val) {
                    _this.text("");
                    _this.addClass("load");
                    _this.addClass("disabled");

                    $.post("/ajax-handler/", {type: "coupon", coupon_name: coupon_val}, response => {
                        const data = JSON.parse(response);

                        if (data.status === "ok") {
                            _this.text("Применить");
                            _this.removeClass("load");
                            _this.removeClass("disabled");

                            if (data.response.response.length) {
                                const coupon = data.response.response[0];

                                if (moment(coupon.expired_at).unix() < moment().unix()) {
                                    alert('Срок действия купона истёк');
                                } else {
                                    apply_coupon_val = coupon.value;
                                    onChangedFinalSum(sum);
                                }
                            } else {
                                alert('Купон "' + coupon_val + '" не найден');
                            }
                        } else {
                            alert("Произошла ошибка. Повторите попытку позже")
                        }
                    })
                }
            });

            /* модалка добавления городов в Геолокацию */

            $selectCitiesModal.select2();

            $.getJSON("/site/templates/cities_dict.json", function(cities) {
                cities.map(city => $selectCitiesModal.append(`<option value="${city.name}">${city.name}</option>`))
            });

            $('#modal-add-cities .btn-primary').click(function (e) {
                const value = $('.select-cities').select2('val');

                if (!$(`.brief-geolocation__cities [data-city="${value}"]`).length) {
                    $('.brief-geolocation__cities').prepend(
                        `<div class="city" data-city="${value}">${value}<div class="city-remove"></div></div>`
                    );

                    const cities_str = [];
                    const $cities = $('.brief-geolocation__cities .city:not(.add-city)');

                    $cities.map(city => cities_str.push($($cities[city]).attr('data-city')));

                    brief_data.ta_locations = cities_str;
                    sessionStorage.setItem('o2o_brief_data', JSON.stringify(brief_data))
                }
            });

            /* инициализация подсказок */
            $(document).tooltip();

            /* инициализация слайдеров */

            $slider_age.slider({
                range: true,
                min: 10,
                max: 100,
                values: [brief_data.ta_age_from, brief_data.ta_age_to],
                create: function() {
                    $age_from.html('<div>' + brief_data.ta_age_from + '</div>');
                    $age_to.html('<div>' + brief_data.ta_age_to + '</div>');
                },
                slide: function(event, ui) {
                    if (ui.handleIndex === 0) {
                        $age_from.html('<div>' + ui.value + '</div>');
                        brief_data.ta_age_from = Number(ui.value);
                    } else {
                        $age_to.html('<div>' + ui.value + '</div>');
                        brief_data.ta_age_to = Number(ui.value);
                    }
                    sessionStorage.setItem('o2o_brief_data', JSON.stringify(brief_data))
                }
            });

            $slider_income.slider({
                range: true,
                min: 0,
                max: 200000,
                values: [brief_data.ta_revenue_from, brief_data.ta_revenue_to],
                create: function() {
                    $revenue_from.html('<div>' + numberWithSpaces(brief_data.ta_revenue_from) + '</div>');
                    $revenue_to.html('<div>' + numberWithSpaces(brief_data.ta_revenue_to) + '</div>');
                },
                slide: function(event, ui) {
                    if (ui.handleIndex === 0) {
                        $revenue_from.html('<div>' + numberWithSpaces(ui.value) + '</div>');
                        brief_data.ta_revenue_from = Number(ui.value);
                    } else {
                        $revenue_to.html('<div>' + numberWithSpaces(ui.value) + '</div>');
                        brief_data.ta_revenue_to = Number(ui.value);
                    }
                    sessionStorage.setItem('o2o_brief_data', JSON.stringify(brief_data))
                }
            });

            function initCalc() {
                $owlCarouselCalc.owlCarousel({
                    loop: false,
                    nav: false,
                    margin: 10,
                    items: 1,
                    mouseDrag: false,
                    touchDrag: false,
                    autoHeight: true,
                    singleItem: true,
                });
            };

            currentStep = Number($('.owl-item.active .brief').attr('data-step'));

            function onClickStep(target_step, direction, trigger = true) {
                const count_all_steps = $owlCarouselCalc.find('.owl-item').length + 1;

                $btn_back.attr('data-step', target_step - 1);

                if (target_step + 1 <= count_all_steps) {
                    $btn_next.attr('data-step', target_step + 1);
                    $briefs.removeClass('hide');
                    $briefResult.removeClass('full');
                    $btn_next.removeClass('disabled');
                } else {
                    $briefs.addClass('hide');
                    $briefResult.addClass('full');
                    $btn_next.addClass('disabled');
                }

                if (Number($btn_back.attr('data-step')) !== 0)
                    $btn_back.removeClass('disabled');
                else
                    $btn_back.addClass('disabled');

                if ((target_step !== 8 || target_step === 8 && direction !== 'prev') && trigger)
                    $owlCarouselCalc.trigger(`${direction}.owl.carousel`);

                currentStep = target_step;

                $('.brief-step.gradient').removeClass('gradient');
                $('.brief-step.active').removeClass('active');
                $(`.brief-step[data-step="${target_step}"]`).addClass('gradient');
                $(`.brief-step[data-step="${target_step}"]`).addClass('active');
                $(`.brief-step[data-step="${target_step}"]`).prevAll('.action').addClass('active');

                if (!$('.owl-item.active .brief').hasClass('visited')) {
                    $('.owl-item.active .brief').addClass('visited')
                }

                if (!$(`.brief-step[data-step="${target_step}"]`).hasClass('visited')) {
                    $(`.brief-step[data-step="${target_step}"]`).addClass('visited')
                }

                if (target_step === 8)
                    $('.brief-result .hint').show();
                else
                    $('.brief-result .hint').hide();

                if (target_step === 3) {
                    onChangePlatform()
                }

                changeOrder()
            }

            function getFormatValue(value, field) {
                if (typeof value === "boolean")
                    return value ? 'да' : 'нет';

                switch (field) {
                    case 'outdoors_object_ids':
                        return value.join(', ');
                    case 'macs_object_ids':
                        return value.join(', ');
                    case 'start':
                        return moment(value).format('DD.MM.YYYY');
                    case 'end':
                        return moment(value).format('DD.MM.YYYY');
                    case 'ta_locations':
                        return value.join(', ');
                    case 'ta_gender':
                        return value === 2 ? 'ж, м' : value === 1 ? 'м' : 'ж';
                    case 'site_type':
                        return value === 1 ? 'Landing Page' : value === 2 ? 'Многостраничный сайт' : 'Интернет-магазин';
                    default:
                        return value
                }
            }

            const browser = detect.parse(navigator.userAgent).browser.family;

            if (browser === 'Safari' || browser === 'Firefox') {
                $('.get-pdf').hide();
            }

            $('.get-pdf').click(function () {
                const _this = $(this);

                $('#coupon').hide();
                $('#checkout').hide();
                $('#brief-footer').hide();
                $('#jvlabelWrap').hide();
                _this.addClass('disabled');

                html2canvas(document.body, {allowTaint:true, useCORS:true}).then(canvas => {
                    const imgData = canvas.toDataURL('image/jpeg', 0.5);
                    const doc = new jsPDF('l', 'mm', [canvas.width, canvas.height]);

                    doc.addImage(imgData, 'JPEG', 0, 0, canvas.width, canvas.height);
                    doc.save('sample-file.pdf');

                    $('#coupon').show();
                    $('#checkout').show();
                    $('#brief-footer').show();
                    $('#jvlabelWrap').show();
                    _this.removeClass('disabled');
                });
            });
        } else if ($page.hasClass('page-reviews')) {
            const $dialogReview = $(".dialog-review");
            const $uiDialog = $dialogReview.closest('.ui-dialog');
            const $uiDialogContent = $dialogReview.closest('.ui-dialog-content');
            const $modalWrapperReview = $('.modal-wrapper-review');

            $('.iframe').fancybox();

            $dialogReview.dialog({
                autoOpen: false,
                draggable: false,
                closeText : '',
                beforeClose: function() {
                    $modalWrapperReview.css('display', 'none');
                },
                open: function() {
                    $modalWrapperReview.css('display', 'block');
                }
            });

            $modalWrapperReview.click(function () {
                $dialogReview.dialog( "close" );
            });

            $('.btn-read').click(function () {
                $dialogReview.find('[name="source"]').val($(this).text());
                $dialogReview.dialog("open");
            });

            $uiDialog.css('max-width', '600px');
            $uiDialog.css('padding', '20px 20px 0');
            $uiDialogContent.css('padding', '20px');
        }
    }

    function getCounts(page, init=true) {
        $.post("/ajax-handler/", {type: "counts"}, data => {
            objects_data = JSON.parse(data);
            cities = objects_data.cities;
            objects = city_name ? objects_data.cities[city_name].objects : null;

            if (init)
                myGroup = new ymaps.GeoObjectCollection({}, {});

            if (page === 'calc' && init) {
                initSelectsAfterRequest();
                myGroup2 = new ymaps.GeoObjectCollection({}, {});
            }

            const types = objects_data.object_types;

            types && types.map(obj => types_obj[obj.id] = obj.name);

            Object.keys(cities).length && Object.keys(cities).map(city => {
                if (page === 'main' && cities_obj[city]) {
                    cities[city].coords = cities_obj[city].coords;
                    cities[city].all = Number(cities[city].all);
                    cities[city].abbr = cities_obj[city].abbreviation;
                }
            });

            if (init) {
                initPlacemarks(page);

                map.geoObjects.add(myGroup);

                if (page === 'main')
                    map.setBounds(myGroup.getBounds());

                if (page === 'calc')
                    map2.geoObjects.add(myGroup2);
            }

            setMapData(page);

            if (init && page === 'calc') {
                setDefaultProps();
            }
        })
    }

    function initPlacemarks(page) {
        if (page === 'main') {
            const content = ymaps.templateLayoutFactory.createClass('');
            const cities_data = Object.assign(cities_obj, cities);

            Object.keys(cities_data).map(city => {
                const placemark = new ymaps.Placemark(cities_data[city].coords, {
                    id: city,
                    href: objects_data.cities[city] ? cities_data[city].abbr : '',
                    hintContent: '<div class="placemark-hint">' +
                        '<div class="city-name">' + city + '</div>' +
                        '<div class="text-gray">Собрано данных</div>' +
                        '<div class="count">' + numberWithSpaces(cities_data[city].all) + '</div>' +
                        '</div>'
                }, {
                    iconLayout: 'default#imageWithContent',
                    iconImageHref: `/site/templates/img/map_point${!cities_data[city].objects ||
                    !cities_data[city].objects.length ? '_disabled' : ''}.png`,
                    iconImageSize: [30, 30],
                    iconImageOffset: [-10, -10],
                    iconContentLayout: content
                });

                placemark.events.add(['click'],  function (e) {
                    const abbr = e.get('target').properties.get('href');

                    if (abbr) {
                        window.location = '/' + abbr;
                    }
                });

                myGroup.add(placemark);
            });
        } else if (page === 'calc' || page === 'map') {
            const content = ymaps.templateLayoutFactory.createClass(
                '<div class="bord-hint">$[properties.iconContent]</div>'
            );

            objects && objects.map(obj => radars_obj[obj.id] = obj);
            objects && objects.map(obj => {
                const type_id = obj.object_type_id;
                const iconImageHref = page === 'calc' ?
                    outdoors_type_ids.indexOf(type_id) !== -1 ?
                        '/site/templates/img/map-bord-gray.png' : '/site/templates/img/still.png' :
                    outdoors_type_ids.indexOf(type_id) !== -1 ?
                        '/site/templates/img/bord.png' : '/site/templates/img/selected.png';
                const center = [obj.location.lng, obj.location.ltd];
                const options = {
                    id: obj.id,
                    object_type_id: type_id,
                    checked: false,
                    iconContent: '0'
                };
                const properties = {
                    iconLayout: 'default#imageWithContent',
                    iconImageHref: iconImageHref,
                    iconImageSize: [20, 21],
                    iconImageOffset: [-10, -10],
                    iconContentOffset: [12, -18],
                    iconContentLayout: content,
                    hintContent: ""
                };
                if (page === 'map') {
                    properties.balloonPanelMaxMapArea = 0;
                    properties.openEmptyBalloon = true;

                    const placemark = new ymaps.Placemark(center, options, properties);

                    placemark.events.add('balloonopen', function (e) {
                        const properties = e.get('target').properties;
                        const id = properties.get('id');

                        properties.set('balloonContent', "Идет загрузка данных...");
                        getOwnerInfo(id, properties)
                    });

                    myGroup.add(placemark);
                } else {
                    const visiblePlacemark1 = type_id === 1;
                    const visiblePlacemark2 = Number($selectTypeObject.find(':selected').val()) !== -1 ?
                        Number($selectTypeObject.find(':selected').val()) === type_id : true;
                    const placemark = new ymaps.Placemark(center, options,
                        Object.assign({}, properties, {visible: visiblePlacemark1}));
                    const placemark1 = new ymaps.Placemark(center, options,
                        Object.assign({}, properties, {visible: visiblePlacemark2}));

                    [placemark, placemark1].map(pm => pm.events
                        .add('mouseenter', function (e) {
                            const target = e.get('target');
                            const properties = target.properties;
                            const options = target.options;
                            const id = properties.get('id');
                            const checked = properties.get('checked');
                            const object_type_id = Number(properties.get('object_type_id'));
                            const iconImageHref = outdoors_type_ids.indexOf(object_type_id) !== -1 ?
                                '/site/templates/img/map-bord-gray-dark.png' : '/site/templates/img/pointed (1).png';

                            if (!checked)
                                options.set('iconImageHref', iconImageHref);

                            properties.set('hintContent', "Идет загрузка данных...");

                            getOwnerInfo(id, properties, false)
                        })
                        .add('mouseleave', function (e) {
                            const target = e.get('target');
                            const properties = target.properties;
                            const checked = properties.get('checked');
                            const object_type_id = Number(properties.get('object_type_id'));
                            const iconImageHref = outdoors_type_ids.indexOf(object_type_id) !== -1 ?
                                '/site/templates/img/map-bord-gray.png' : '/site/templates/img/still.png';

                            if (!checked)
                                target.options.set('iconImageHref', iconImageHref);
                        })
                        .add('click', function (e) {
                            const target = e.get('target');
                            const properties = target.properties;
                            const checked = properties.get('checked');
                            const id = Number(properties.get('id'));
                            const object_type_id = Number(properties.get('object_type_id'));
                            const iconImageHref = outdoors_type_ids.indexOf(object_type_id) !== -1 ?
                                `/site/templates/img/${checked ? 'map-bord-gray' : 'bord'}.png` : `/site/templates/img/${checked ? 'still' : 'selected'}.png`;

                            properties.set('checked', !checked);
                            target.options.set('iconImageHref', iconImageHref);

                            onClickPlacemark(checked, id);
                        })
                    );

                    if (type_id === 1 && obj.owner_id === 9)
                        myGroup.add(placemark);

                    myGroup2.add(placemark1);
                }
            });
        }
    }

    function setDefaultProps() {
        if (!brief_data_storage) {
            $systemAnalyticsNo.prop('defaultChecked', true);
            $multiPage.prop('defaultChecked', true);
            $hasSiteNo.prop('defaultChecked', true);
            $('.ca-trust[data-answer="no"]').prop('defaultChecked', true);
            $('.has-creatives [data-answer="no"]').prop('defaultChecked', true);
            $('[name="support"][data-answer="unique"]').prop('defaultChecked', true);
            $('[name="interval"][data-answer="1-week"]').prop('defaultChecked', true);
            datepicker.selectDate([start_default, end_default]);
        } else {
            $(`[data-step="1"] .list-selected-objects`).empty();
            $(`[data-step="2"] .list-selected-objects`).empty();

            myGroup.each(function (el) {
                const properties = el.properties;
                const id = properties.get('id');

                if (brief_data.outdoors_object_ids.indexOf(id) !== -1) {
                    properties.set('checked', true);
                    el.options.set('iconImageHref', `/site/templates/img/bord.png`);

                    const $obj = $('<li>', {"data-id": id});
                    $obj.append(`<div class="obj-remove"></div>${radars_obj[id].name}`);
                    $obj.click(function () {
                        onClickPlacemark(true, Number($(this).attr('data-id')))
                    });

                    $(`[data-step="1"] .list-selected-objects`).append($obj);
                }
            });

            $('.brief[data-step="1"] .total-selected .count').text(brief_data.outdoors_object_ids.length);

            if (brief_data.outdoors_object_ids.length > 1)
                $(`[data-step="1"] .btn-remove-all`).show();
            else
                $(`[data-step="1"] .btn-remove-all`).hide();

            myGroup2.each(function (el) {
                const properties = el.properties;
                const id = properties.get('id');
                const object_type_id = Number(properties.get('object_type_id'));
                const iconImageHref = outdoors_type_ids.indexOf(object_type_id) !== -1 ?
                    `/site/templates/img/bord.png` : `/site/templates/img/selected.png`;

                if (brief_data.macs_object_ids.indexOf(id) !== -1) {
                    properties.set('checked', true);
                    el.options.set('iconImageHref', iconImageHref);

                    const $obj = $('<li>', {"data-id": id});
                    $obj.append(`<div class="obj-remove"></div>${radars_obj[id].name}`);
                    $obj.click(function () {
                        onClickPlacemark(true, Number($(this).attr('data-id')))
                    });

                    $(`[data-step="2"] .list-selected-objects`).append($obj);
                }
            });

            $('.brief[data-step="2"] .total-selected .count').text(brief_data.macs_object_ids.length);

            if (brief_data.macs_object_ids.length > 1)
                $(`[data-step="2"] .btn-remove-all`).show();
            else
                $(`[data-step="2"] .btn-remove-all`).hide();

            datepicker.selectDate([moment.unix(brief_data.start).toDate(), moment.unix(brief_data.end).toDate()]);

            if (brief_data.platforms_auto) {
                $advPlatformTrust.prop('defaultChecked', true);
                $('.brief[data-step="3"] [data-hide]').toggleClass('disabled');
            } else {
                if (brief_data.platforms_yandex)
                    $platforms_yandex.prop('defaultChecked', true);

                if (brief_data.platforms_facebook)
                    $platforms_facebook.prop('defaultChecked', true);

                if (brief_data.platforms_my_target)
                    $platforms_my_target.prop('defaultChecked', true);

                if (brief_data.platforms_google)
                    $platforms_google.prop('defaultChecked', true);

                if (brief_data.platforms_youtube)
                    $platforms_youtube.prop('defaultChecked', true);
            }

            const $blockHide = $('.brief[data-step="4"] [data-hide]');

            if (brief_data.ta_auto) {
                $blockHide.addClass('disabled');
                $('.ca-trust[data-answer="yes"]').prop('defaultChecked', true);
            } else {
                $blockHide.removeClass('disabled');
                $('.ca-trust[data-answer="no"]').prop('defaultChecked', true);
            }

            if (brief_data.ta_gender === 0) {
                $female.prop('defaultChecked', true);
            } else if (brief_data.ta_gender === 1) {
                $male.prop('defaultChecked', true);
            }

            brief_data.ta_locations.map(city => {
                $('.brief-geolocation__cities').prepend(
                    `<div class="city" data-city="${city}">${city}<div class="city-remove"></div></div>`
                );
            });

            $ta_additional_info.val(brief_data.ta_additional_info);

            if (brief_data.site_need) {
                $hasSiteNo.prop('defaultChecked', true);
                $priceSiteBlock.show();
                $typeSite.addClass('disabled');
                $systemAnalyticsYes.closest('.cont').addClass('disabled');
            } else {
                $hasSiteYes.prop('defaultChecked', true);
                $priceSiteBlock.hide();
                $typeSite.removeClass('disabled');
                $systemAnalyticsYes.closest('.cont').removeClass('disabled');
            }

            if (brief_data.site_type === 1) {
                $singlePage.prop('checked', true);
                $('[name="price-site"][data-answer="50000"]').prop('defaultChecked', true)
            } else {
                $multiPage.prop('checked', true);

                if (brief_data.site_type === 2)
                    $('[name="price-site"][data-answer="100000"]').prop('defaultChecked', true)

                if (brief_data.site_type === 3)
                    $('[name="price-site"][data-answer="300000"]').prop('defaultChecked', true)
            }

            if (brief_data.site_need_help) {
                $systemAnalyticsNo.prop('defaultChecked', true);
            } else {
                $systemAnalyticsYes.prop('defaultChecked', true);
            }

            if (brief_data.site_yandex)
                $siteYandex.prop('defaultChecked', true);

            if (brief_data.site_targets)
                $siteTargets2.prop('defaultChecked', true);

            if (brief_data.site_google)
                $siteGoogle.prop('defaultChecked', true);

            if (brief_data.site_call_tracking)
                $siteCallTracking.prop('defaultChecked', true);

            if (brief_data.banners_creative_company) {
                $createCompany.prop('defaultChecked', true);
                $blockDesign.addClass('disabled');
            }

            if (brief_data.banners_need_adaptation) {
                $layoutsYesNo.prop('defaultChecked', true);
                $('.count-layouts .select-custom__option span').text(numberWithSpaces(brief_data.banners_creatives_count));
                $('.count-layouts option[value="' + brief_data.banners_creatives_count + '"]').attr("selected", true);

                $selectCountLayouts.closest('.select-custom')
                    .find('.select-custom__option span')
                    .text($($selectCountLayouts.find(':selected')[0]).text());
                $supportDesigner.addClass('disabled');
                $countLayouts.show();
            } else {
                $('.count-creatives .select-custom__option span').text(numberWithSpaces(brief_data.banners_creatives_count));
                $('.count-creatives option[value="' + brief_data.banners_creatives_count + '"]').attr("selected", true);

                if (brief_data.banners_exist) {
                    $layoutsYes.prop('defaultChecked', true);
                    $supportDesigner.addClass('disabled');
                } else {
                    $layoutsNo.prop('defaultChecked', true);
                    $supportDesigner.removeClass('disabled');
                }

                $countLayouts.hide();
            }

            if (brief_data.banners_template) {
                $supportTemplate.prop('defaultChecked', true);
                $countCreatives.hide();
            } else {
                $('[name="support"][data-answer="unique"]').prop('defaultChecked', true);
                $countCreatives.show();
            }

            if (brief_data.banners_video) {
                $createVideo.prop('defaultChecked', true);
            }

            $('.count-shows .select-custom__option span').text(numberWithSpaces(brief_data.shows_count));
            $('.count-shows option[value="' + brief_data.shows_count + '"]').attr("selected", true);

            if (brief_data.shows_runtime_weeks !== 4) {
                $('[name="interval"][data-answer="' + brief_data.shows_runtime_weeks + '-week"]').prop('defaultChecked', true);
            } else {
                $('[name="interval"][data-answer="1-month"]').prop('defaultChecked', true);
            }

            if (brief_data.smm_create_vk)
                $blockSocialVkCreate.prop('defaultChecked', true);

            if (brief_data.smm_create_instagram)
                $blockSocialInstCreate.prop('defaultChecked', true);

            if (brief_data.smm_create_facebook)
                $blockSocialFacebookCreate.prop('defaultChecked', true);

            if (brief_data.smm_management_vk)
                $blockSocialVkVedenie.prop('defaultChecked', true);

            if (brief_data.smm_management_instagram)
                $blockSocialInstVedenie.prop('defaultChecked', true);

            if (brief_data.smm_management_facebook)
                $blockSocialFacebookVedenie.prop('defaultChecked', true);

            if (brief_data.smm_decor_vk)
                $blockSocialVkDecor.prop('defaultChecked', true);

            if (brief_data.smm_decor_instagram)
                $blockSocialInstDecor.prop('defaultChecked', true);

            if (brief_data.smm_decor_facebook)
                $blockSocialFacebookDecor.prop('defaultChecked', true);

            if (brief_data.smm_create_content)
                $supportContentYes.prop('defaultChecked', true);
            else
                $supportContentNo.prop('defaultChecked', true);

            if (brief_data.serm_reputation)
                $blockReputationNegative.prop('defaultChecked', true);

            if (brief_data.serm_feedbacks)
                $blockReputationReviews.prop('defaultChecked', true);
        }
    }

    function onClickPlacemark(checked, id, isDeleteAll=false) {
        if (currentStep === 1) {
            if (!checked) {
                brief_data.outdoors_object_ids.push(id);
            } else {
                brief_data.outdoors_object_ids.splice(brief_data.outdoors_object_ids.indexOf(id), 1);
            }
        } else {
            if (!checked) {
                brief_data.macs_object_ids.push(id)
            } else {
                brief_data.macs_object_ids.splice(brief_data.macs_object_ids.indexOf(id), 1)
            }
        }

        if (!checked) {
            const $obj = $('<li>', {"data-id": id});
            $obj.append(`<div class="obj-remove"></div>${radars_obj[id].name}`);
            $obj.click(function () {
                onClickPlacemark(true, Number($(this).attr('data-id')))
            });

            $(`[data-step="${currentStep}"] .list-selected-objects`).append($obj)
        } else {
            $(`[data-step="${currentStep}"] .list-selected-objects`).find(`[data-id="${id}"]`).remove();
        }

        (currentStep === 1 ? myGroup : myGroup2).each(function (el) {
            const properties = el.properties;

            if (properties.get('id') === Number(id)) {
                const object_type_id = Number(properties.get('object_type_id'));
                const iconImageHref = outdoors_type_ids.indexOf(object_type_id) !== -1 ?
                    `/site/templates/img/${checked ? 'map-bord-gray' : 'bord'}.png` : `/site/templates/img/${checked ? 'still' : 'selected'}.png`;

                properties.set('checked', !checked);
                el.options.set('iconImageHref', iconImageHref);
            }
        });

        if (currentStep === 1)
            brief_data.outdoors_object_ids = Array.from(new Set(brief_data.outdoors_object_ids));
        else
            brief_data.macs_object_ids = Array.from(new Set(brief_data.macs_object_ids));

        $('.brief[data-step="' + currentStep + '"] .total-selected .count').text(
            currentStep === 1 ? brief_data.outdoors_object_ids.length : brief_data.macs_object_ids.length);

        if (brief_data.outdoors_object_ids.length || brief_data.macs_object_ids.length)
            $('.brief-geolocation').hide();
        else
            $('.brief-geolocation').show();

        if ((currentStep === 1 ? brief_data.outdoors_object_ids : brief_data.macs_object_ids).length > 1)
            $(`[data-step="${currentStep}"] .btn-remove-all`).show();
        else
            $(`[data-step="${currentStep}"] .btn-remove-all`).hide();

        if (currentStep === 2)
            getCalculateAll(brief_data.start, brief_data.end, isDeleteAll);
        else
            changeOrder();
    }

    function getCalculateObjects(start, end) {
        const loader = $('.brief[data-step="2"] .loader-wrapper');
        loader.removeClass('hidden');

        const object_ids = [];

        Object.keys(radars_obj).map(id => {
            object_ids.push(Number(id))
        });

        if (object_ids.length && start) {
            $.post("/ajax-handler/", {type: "calculate_objects", start, end, object_ids}, data => {
                data = JSON.parse(data);

                if (data.status === "ok") {
                    data.response.map(radar => {
                        radars_obj[radar.object_id].count_macs = radar.count_macs
                    });

                    bruteForceGroup();
                } else {
                    console.error(data)
                }

                loader.addClass('hidden');
            })
        } else {
            loader.addClass('hidden');
        }

        bruteForceGroup()
    }

    function bruteForceGroup() {
        myGroup2 && myGroup2.each(function (el) {
            const id = el.properties.get('id');

            el.properties.set('iconContent', radars_obj[id] ? numberWithSpaces(radars_obj[id].count_macs) : 0);
        });
    }

    function onChangePlatform(auto=false) {
        if (auto)
            $('.brief[data-step="3"] [data-hide]').toggleClass('disabled');

        if (!$advPlatformTrust.prop('checked') && !$platforms_yandex.prop('checked') &&
            !$platforms_my_target.prop('checked') && !$platforms_facebook.prop('checked') &&
            !$platforms_google.prop('checked') && !$platforms_youtube.prop('checked')) {
            $btn_next.addClass('disabled')
        } else {
            $btn_next.removeClass('disabled')
        }

        brief_data.platforms_auto = $advPlatformTrust.prop('checked');
        brief_data.platforms_yandex = $platforms_yandex.prop('checked');
        brief_data.platforms_my_target = $platforms_my_target.prop('checked');
        brief_data.platforms_facebook = $platforms_facebook.prop('checked');
        brief_data.platforms_google = $platforms_google.prop('checked');
        brief_data.platforms_youtube = $platforms_youtube.prop('checked');
    }

    function getCalculateAll(start, end, isDeleteAll=false) {
        const $count = $('.count-all-macs .count');
        $count.html('<img class="loader" src="/site/templates/img/loader.svg" alt="">');

        if (brief_data.macs_object_ids.length && start && !isDeleteAll) {
            $.post("/ajax-handler/", {type: "calculate_all", start, end, object_ids: brief_data.macs_object_ids}, data => {
                data = JSON.parse(data);

                if (data.status === "ok") {
                    brief_data.macs_count = Number(data.response);
                    $count.html(numberWithSpaces(data.response))
                } else {
                    console.error(data);
                    brief_data.macs_count = 0;
                    $count.html(0)
                }

                changeOrder()
            })
        } else {
            brief_data.macs_count = 0;
            $count.html(0);
            changeOrder()
        }
    }

    function changeOrder() {
        const final_data = getFinalSum();
        const sum = final_data[0];
        const order = final_data[1];

        $finalSum.text(numberWithSpaces(sum.toFixed(0)));

        $briefOrder.empty();
        order.map(service => {
            $briefOrder.append(
                '<div class="service">' +
                '<div class="service-name">' + service.title + '</div>' +
                '<div class="service-border"></div>' +
                '<div class="service-price">' + service.price + ' ₽</div>' +
                '</div>'
            )
        });

        sessionStorage.setItem('o2o_brief_data', JSON.stringify(brief_data));

        if (apply_coupon_val)
            onChangedFinalSum(sum)
    }

    function onChangedFinalSum(sum) {
        $finalSum.text(numberWithSpaces((sum - sum * (Number(apply_coupon_val) / 100)).toFixed(0)));
        $finalSumOld.text(numberWithSpaces(sum.toFixed(0)));
    }

    function getFinalSum() {
        const order = [];
        const outdoors_length = brief_data.outdoors_object_ids.length;
        const $step2 = $('.brief[data-step="2"]');
        const $step3 = $('.brief[data-step="3"]');
        const $step5 = $('.brief[data-step="5"]');
        const $step6 = $('.brief[data-step="6"]');
        const $step7 = $('.brief[data-step="7"]');
        const $step8 = $('.brief[data-step="8"]');
        let price_service = 0;
        let sum = 0;

        if (outdoors_length > 0 && outdoors_length <= 5)
            price_service = outdoors_length * 15000;
        else if (outdoors_length > 5 && outdoors_length <= 10)
            price_service = outdoors_length * 12000;
        else if (outdoors_length > 10 && outdoors_length <= 12)
            price_service = outdoors_length * 10000;

        sum = addOrder(sum, order, `Реклама на видеоэкранах (${outdoors_length})`, price_service);

        if ($step2.hasClass('visited') && brief_data.macs_count)
            sum = addOrder(sum, order, `Данные О2О (${numberWithSpaces(brief_data.macs_count)})`,
                (0.1 * brief_data.macs_count).toFixed(0));

        if ($step3.hasClass('visited')) {
            let adv_sites = [];
            price_service = 0;

            if (brief_data.platforms_auto) {
                price_service += 30000;
                adv_sites = ['Яндекс', 'MyTarget', 'Facebook', 'Google', 'YouTube'];
            } else {
                if (brief_data.platforms_yandex) {
                    price_service += 10000;
                    adv_sites.push('Яндекс');
                }

                if (brief_data.platforms_my_target) {
                    price_service += 10000;
                    adv_sites.push('MyTarget');
                }

                if (brief_data.platforms_facebook) {
                    price_service += 10000;
                    adv_sites.push('Facebook');
                }

                if (brief_data.platforms_google) {
                    price_service += 10000;
                    adv_sites.push('Google');
                }

                if (brief_data.platforms_youtube) {
                    price_service += 10000;
                    adv_sites.push('YouTube');
                }
            }

            sum = addOrder(sum, order, `Настройка рекламной кампании (${adv_sites.join(', ')})`, price_service);
        }

        if ($step5.hasClass('visited')) {
            if ($hasSiteNo.prop('checked')) {
                price_service = Number($('[name="price-site"]:checked').attr('data-answer'));
                const type_site = price_service === 50000 ?
                    'Landing page' : price_service === 100000 ?
                        'Многостраничный сайт' :
                        price_service === 300000 ?
                            'Интернет-магазин' : '';

                sum = addOrder(sum, order, `Разработка сайта (${type_site})`, price_service);
            }

            if ($step6.hasClass('visited')) {
                if (brief_data.site_need_help) {
                    price_service = 0;

                    if (brief_data.site_google) {
                        if ($singlePage.prop('checked'))
                            price_service += 500;
                        else
                            price_service += 2500;
                    }

                    if (brief_data.site_yandex) {
                        if ($singlePage.prop('checked'))
                            price_service += 500;
                        else
                            price_service += 2500;
                    }

                    if (brief_data.site_targets) {
                        if ($singlePage.prop('checked'))
                            price_service += 500;
                        else
                            price_service += 2500;
                    }

                    if (brief_data.site_call_tracking)
                        price_service += 6000;


                    sum = addOrder(sum, order, 'Настройка аналитики', price_service);
                }
            }
        }

        if ($step7.hasClass('visited')) {
            price_service = 0;

            if (brief_data.banners_creative_company)
                price_service += 50000;
            else {
                if (brief_data.banners_need_adaptation)
                    price_service += brief_data.banners_creatives_count * 500;
                else if ($layoutsNo.prop('checked')) {
                    if (brief_data.banners_template)
                        price_service += 500;
                    else
                        price_service += brief_data.banners_creatives_count * 1000;
                }

                if (brief_data.banners_video)
                    price_service += 5000;
            }

            sum = addOrder(sum, order, 'Дизайн рекламной кампании', price_service);

            let title = '';
            const count_shows = brief_data.shows_count;
            const interval = $('[name="interval"]:checked').attr('data-answer');
            price_service = 0;

            if (interval === '1-week') {
                price_service = count_shows / 100 * 15;
                title = '1 неделя';
            } else if (interval === '2-week') {
                price_service = count_shows / 10;
                title = '2 недели';
            } else if (interval === '1-month') {
                price_service = count_shows / 100 * 5;
                title = '1 месяц';
            }

            sum = addOrder(sum, order,
                `Длительность рекламной кампании (${title} – ${numberWithSpaces(count_shows)} показов)`,
                price_service);
        }

        if ($step8.hasClass('visited')) {
            const services = [];
            let socials = [];
            price_service = 0;

            if (brief_data.smm_create_vk) {
                price_service += 2500;
                socials.push('vk')
            }

            if (brief_data.smm_create_instagram) {
                price_service += 2500;
                socials.push('instagram')
            }

            if (brief_data.smm_create_facebook) {
                price_service += 2500;
                socials.push('facebook')
            }

            if (socials.length)
                services.push(`cоздание - ${socials.join(', ')}`);

            socials = [];

            if (brief_data.smm_management_vk) {
                price_service += 15000;
                socials.push('vk')
            }

            if (brief_data.smm_management_instagram) {
                price_service += 15000;
                socials.push('instagram')
            }

            if (brief_data.smm_management_facebook) {
                price_service += 15000;
                socials.push('facebook')
            }

            if (socials.length)
                services.push(`ведение (1 мес.) - ${socials.join(', ')}`);

            socials = [];

            if (brief_data.smm_decor_vk) {
                price_service += 15000;
                socials.push('vk')
            }

            if (brief_data.smm_decor_instagram) {
                price_service += 15000;
                socials.push('instagram')
            }

            if (brief_data.smm_decor_facebook) {
                price_service += 15000;
                socials.push('facebook')
            }

            if (socials.length)
                services.push(`оформление - ${socials.join(', ')}`);

            if (brief_data.smm_create_content) {
                price_service += 15000;
                services.push('создание контента (1 мес.)')
            }

            if (brief_data.serm_reputation || brief_data.serm_feedbacks) {
                if (brief_data.serm_reputation)
                    price_service += 10000;

                if (brief_data.serm_feedbacks)
                    price_service += 5000;

                services.push('управление репутацией')
            }

            sum = addOrder(sum, order, `SMM (${services.join(', ')})`, price_service)
        }

        return [sum, order]
    }

    function addOrder(sum, order, title, price) {
        if (price) {
            sum += Number(price);
            order.push({title, price: numberWithSpaces(price)});
        }

        return sum
    }

    /* инициализация календаря */
    const datepicker = $period.datepicker({
        onSelect: function(fd, date) {
            brief_data.start = moment(date[0]).unix();
            brief_data.end = date[1] ? moment(date[1]).endOf('day').unix() :
                moment(date[0]).endOf('day').unix();

            getCalculateAll(brief_data.start, brief_data.end);
            getCalculateObjects(brief_data.start, brief_data.end);

            sessionStorage.setItem('o2o_brief_data', JSON.stringify(brief_data));
        }
    }).data('datepicker');

    function setMapData(page) {
        const d = new Date();
        const month = page !== 'map' ? d.getMonth() + 1 : $(`#months .owl-item .active`).data('number');
        const year = d.getFullYear();
        const date = (Number(month) > 9 ? month.toString() : '0' + month.toString()) + year.toString()[2] + year.toString()[3];
        const counts = page !== 'main' && objects_data.cities[city_name][`counts_${date}`] ?
            JSON.parse(objects_data.cities[city_name][`counts_${date}`]) : [];

        if (page === 'calc' || page === 'map') {
            if (page === 'calc')
                $('.map-hint .month').text(`${months_obj[month]} ${year}`);

            if (counts.length) {
                const count_object_macs = {};
                counts.map(count => count_object_macs[count.object_id] = count.count_macs);

                Object.keys(radars_obj).map(id => count_object_macs[id] ?
                    radars_obj[id].count_macs = count_object_macs[id] :
                    radars_obj[id].count_macs = 0
                )
            } else {
                Object.keys(radars_obj).map(id => radars_obj[id].count_macs = 0)
            }
        }

        if (page === 'map' || page === 'main') {
            if (showAllMacs) {
                $('#unique-macs').text(page === 'main' ? objects_data.all : objects_data.cities[city_name].all);
                countup('unique-macs');
            } else {
                $('#unique-macs').text(
                    numberWithSpaces(page === 'main' ? objects_data.all : objects_data.cities[city_name].all)
                );
            }

            if (page === 'map') {
                $('#unique-macs + .text2 span').text(declOfNum(Number(objects_data.all),
                    ['человеке', 'людях', 'людях']));
                $('#unique-macs + .text2 .city').text(city_name);
                $('.map-wrapper').addClass('hidden');
                $('.hint.hidden').removeClass('hidden');
            }

            showAllMacs = false;
            oldAllMacs = objects_data.all;
        }

        (page === 'calc' ? [myGroup, myGroup2] : [myGroup]).map(group => group && group.each(function (el) {
            const properties = el.properties;
            const id = properties.get('id');

            if (page === 'main') {
                const hintContent = '<div class="placemark-hint">' +
                    '<div class="city-name">' + id + '</div>' +
                    '<div class="text-gray">Собрано данных</div>' +
                    '<div class="count">' + numberWithSpaces(cities[id] ? cities[id].all : 0) + '</div>' +
                '</div>';

                properties.set('hintContent', hintContent);
            } else {
                properties.set('iconContent', radars_obj[id] ? numberWithSpaces(radars_obj[id].count_macs) : 0);
            }
        }));
    }

    function initSelectsAfterRequest() {
        const types = objects_data.object_types;

        Object.keys(cities_obj).sort()
            .filter(city => cities_obj[city].abbreviation)
            .map(city => {
                $selectCityLabel.text('Тюмень');
                $selectCity.append(
                    `<option value="${cities_obj[city].coords}" ${city.toLowerCase() === 'тюмень' ? 'selected' : ''}>${city}</option>`
                )
            });

        types && types.map((type, key) => {
            if (key === 0)
                $selectTypeObjectLabel.text('Все типы');

            $selectTypeObject.append(
                key === 0 ?
                    `<option value="-1" selected>Все типы</option>` :
                    `<option value="${type.id}" ${key === 0 ? 'selected' : ''}>${type.name}</option>`
            )
        });
    }

    function getOwnerInfo(id, properties, showPhoto=true) {
        $.post("/ajax-handler/", {type: "owner_info", object_id: id}, response => {
            const data = JSON.parse(response);
            const $blockPhotos = $('.ymaps-2-1-74-balloon__content > ymaps');

            if ((data.response.code === 204 || data.response.code === 200) && radars_obj[id]) {
                $blockPhotos.removeClass('balloon-width');

                const hasOwner = data.response.code !== 204;
                const object_type_id = radars_obj[id].object_type_id;
                const photo = showPhoto && radars_obj[id].photos.length ? radars_obj[id].photos[0] : null;

                const newContent = '<table class="balloon-content">' +
                    '<tr>' +
                    '<td class="left">' +
                    (types_obj[object_type_id] ? ('<div class="balloon-content__type">' + types_obj[object_type_id] + '</div>') : '') +
                    '<div class="balloon-content__count-macs">' +
                    '<div class="label">Собранные данные:</div>' +
                    '<div class="value">' + (radars_obj[id] ? numberWithSpaces(radars_obj[id].count_macs) : 0) + '</div>' +
                    '</div>' + (hasOwner ?
                        ('<div class="balloon-content__owner">' +
                            '<div class="label">Владелец</div>' +
                            '<div class="name">' + data.response.response.name + '</div>' +
                            '<a class="phone" href="tel:' + data.response.response.phone + '">' + formatNumber(data.response.response.phone) + '</a>' +
                            '<a class="site" href="' + data.response.response.site + '" target="_blank">' + data.response.response.site + '</a>' +
                            '</div>') :
                        '<div class="balloon-content__owner"><div class="label">Владелец</div><div class="name"></div>Отсутствует</div>') +
                    '</td>' +
                    (photo && showPhoto ? ('<td class="right"><img src="' + photo.link + '" alt="" /></td>') : '') +
                    '</tr>' +
                    '</table>';

                if (photo && showPhoto)
                    $blockPhotos.addClass('balloon-width');

                properties.set(showPhoto ? 'balloonContent' : 'hintContent', newContent);
            } else {
                properties.set(showPhoto ? 'balloonContent' : 'hintContent', 'Не удалось получить данные. Повторите попытку позже');
            }
        });
    }

    function countup(className) {
        const countBlockTop = $("#" + className).offset().top;
        const windowHeight = window.innerHeight;
        let show = true;

        $(window).scroll(function () {
            if (show && (countBlockTop < $(window).scrollTop() + windowHeight)) {
                show = false;

                $("#" + className).spincrement({
                    duration: 3000,
                    thousandSeparator: ' ',
                });
            }
        })
    }

    function numberWithSpaces(x) {
        return x ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") : 0;
    }

    $('.outside-box').on('click', function() {
        const $this = $(this);

        if ($this.hasClass('contacts')) {
            $('.contacts.curtain').addClass('active');
            $this.addClass('active');

            setTimeout( function() {
                $('.contacts-content .phone, .contacts-content .mail, .contacts-content .address, .contacts-content .social').addClass('animated fadeIn');
            }, 1200 );
        }

        if ($this.hasClass('main-menu')) {
            $('.menu.curtain').addClass('active');
            $('.outside-box.menu').addClass('active');
            $this.addClass('active');

            setTimeout( function() {
                $('nav li, .languages, .nav-footer .social').addClass('animated fadeIn');
            }, 1200 );
        }

        $('footer .year').text(moment().format('YYYY'))
    });

    $('.curtain').find('.background').on('click', function(e) {
        const $this = $(this);

        if($(e.target).is('.contacts-link')){
            e.preventDefault().stopPropagation();
            return false;
        }

        $this.closest('.curtain')
            .removeClass('active');
        $('.outside-box').removeClass('active');

        $('.nav-contacts').fadeOut(400, function() {
            $('nav').fadeIn();
        });

    }).find('nav').on('click', function(e) {
        if(!$(e.target).is('a')){
            e.stopPropagation();
        }
    });

    if (document.getElementById('scene_1'))
        new Parallax(document.getElementById('scene_1'));
    if (document.getElementById('scene_2'))
        new Parallax(document.getElementById('scene_2'));
    if (document.getElementById('scene_3'))
        new Parallax(document.getElementById('scene_3'));
    if (document.getElementById('scene_4'))
        new Parallax(document.getElementById('scene_4'));
    if (document.getElementById('scene_5'))
        new Parallax(document.getElementById('scene_5'));
    if (document.getElementById('scene_6'))
        new Parallax(document.getElementById('scene_6'));
    if (document.getElementById('scene_7'))
        new Parallax(document.getElementById('scene_7'));
    if (document.getElementById('scene-menu'))
        new Parallax(document.getElementById('scene-menu'));
    if (document.getElementById('scene-contacts'))
        new Parallax(document.getElementById('scene-contacts'));

    $(".dialog-connection").dialog({
        autoOpen: false,
        draggable: false,
        closeText : '',
        beforeClose: function() {
            $('.modal-wrapper-connection').css('display', 'none');
        },
        open: function() {
            $('.modal-wrapper-connection').css('display', 'block');
        }
    });

    $('.modal-wrapper-connection').click(function () {
        $(".dialog-connection").dialog( "close" );
    });

    $('.btn-connect').click(function () {
        $(".dialog-connection").find('[name="source"]').val($(this).text());
        $(".dialog-connection").dialog("open");
    });

    if (location.search.indexOf("form") !== -1) {
        $(".dialog-connection").find('[name="source"]').val('Получить консультацию');
        $(".dialog-connection").dialog("open");
    }

    $("form:not(.form-invoice):not(.form-params)").on("submit", function() {
        event.preventDefault();

        $.ajax({
            type: "POST",
            url: '/ajax-handler/',
            data: $(this).serialize(),
            success: () => {
                alert('Заявка успешно отправлена');
                $(".dialog-connection").dialog("close")
            },
            error: () => {
                alert('Произошла ошибка. Повторите попытку позже')
            }
        });
    });

    if (!$page.hasClass('page-calc')) {

        $('.dialog-connection input[name="phone"]').mask("+7-999-999-99-99");

        const phone = $('.call-me #phone');
        if (phone) {
            phone.mask("+7-999-999-99-99");
        }
    }

    function declOfNum(number, titles) {
        const cases = [2, 0, 1, 1, 1, 2];
        return titles[(number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5]];
    }

    $('.header-menu a, footer .menu a, footer .device a, .arrow-continue').bind("click", function(e){
        const anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 1000);
        e.preventDefault();
    });

    $('#scroll-top').click(function() {
        $('html, body').animate({scrollTop: 0}, 700);
        return false;
    });

    function formatNumber(number) {
        const result = number.split("");

        result.splice(0,0,"+");
        result.splice(2,0," (");
        result.splice(6,0,") ");
        result.splice(10,0,"-");
        result.splice(13,0,"-");

        return result.join("")
    }

    $('.burger').click(function () {
        $(this).toggleClass('active');
        $('.header').toggleClass('open');
    });
});