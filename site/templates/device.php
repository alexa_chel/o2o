<?php namespace ProcessWire; ?>

<div class="page page-device">
    <a id="scroll-top">
        <img src="<?= $config->urls->templates ?>img/arrow-scroll-top.png" alt="">
    </a>

    <div class="cooperation-options">
        <div class="container">
            <div id="scene_1">
                <div class="first" data-depth="0.2">Варианты</div>
                <div class="second" data-depth="0.6">сотрудничества</div>
            </div>

            <h2 class="wow fadeInDown">Варианты сотрудничества</h2>

            <div class="row">
                <div class="col wow fadeInLeft">
                    <div class="text1">Приобретение оборудования</div>
                    <div class="text2"><span>+</span>доступ к платформе управления данными</div>
                </div>
                <div class="col wow fadeInRight">
                    <div class="text1">Длительная аренда оборудования</div>
                    <div class="text2"><span>+</span>доступ к платформе управления данными</div>
                </div>
            </div>

            <div class="row">
                <div class="col wow fadeInLeft">
                    <div class="text3">Разовый платёж</div>
                    <div class="price1">10 000</div>
                    <div class="text4">Стоимость оборудования</div>
                    <div class="text5">Абонентская плата</div>
                    <div class="price2">2 500</div>
                    <p class="text6">Включает в себя сотовую связь, сбор, хранение, обработку данных и пользование личным кабинетом с возможностью прямой выгрузки данных на рекламные площадки MyTarget и Яндекс</p>
                </div>
                <div class="col wow fadeInRight">
                    <div class="text3">Абонентская плата</div>
                    <div class="price1">5 000</div>
                    <div class="text4">Включает в себя аренду оборудования, сотовую связь, сбор, хранение, обработку данных и пользование личным кабинетом, с возможностью прямой выгрузки данных на рекламные площадки MyTarget и Яндекс</div>
                    <div class="text8">Минимальный срок договора</div>
                    <div class="text9">6</div>
                </div>
            </div>

            <div class="row wow fadeInDown">
                <div class="col">
                    <div class="text7">* Цены указаны из расчета за один комплект оборудования</div>
                </div>
                <div class="col">
                    <div class="text7">* Цены указаны из расчета за один комплект оборудования</div>
                </div>
            </div>

            <div class="row wow fadeInDown">
                <div class="col">
                    <button class="btn-primary btn-connect">Купить оборудование</button>
                </div>
                <div class="col">
                    <button class="btn-primary btn-connect">Арендовать оборудование</button>
                </div>
            </div>
        </div>
    </div>

    <div class="cooperation-options mobile">
        <div class="container">
            <div id="scene_1">
                <div class="first" data-depth="0.2">Варианты</div>
                <div class="second" data-depth="0.6">сотрудничества</div>
            </div>

            <h2 class="wow fadeInDown">Варианты сотрудничества</h2>

            <div class="row">
                <div class="col">
                    <div class="text1">Приобретение оборудования</div>
                    <div class="text2"><span>+</span>доступ к платформе управления данными</div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="text3">Разовый платёж</div>
                    <div class="price1">10 000</div>
                    <div class="text4">Стоимость оборудования</div>
                    <div class="text5">Абонентская плата</div>
                    <div class="price2">2 500</div>
                    <p class="text6">Включает в себя сотовую связь, сбор, хранение, обработку данных и пользование личным кабинетом с возможностью прямой выгрузки данных на рекламные площадки MyTarget и Яндекс</p>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="text7">* Цены указаны из расчета за один комплект оборудования</div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <button class="btn-primary btn-connect">Купить оборудование</button>
                </div>
            </div>
        </div>
    </div>

    <div class="cooperation-options mobile">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="text1">Длительная аренда оборудования</div>
                    <div class="text2"><span>+</span>доступ к платформе управления данными</div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="text3">Абонентская плата</div>
                    <div class="price1">5 000</div>
                    <div class="text4">Включает в себя аренду оборудования, сотовую связь, сбор, хранение, обработку данных и пользование личным кабинетом, с возможностью прямой выгрузки данных на рекламные площадки MyTarget и Яндекс</div>
                    <div class="text8">Минимальный срок договора</div>
                    <div class="text9">6</div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="text7">* Цены указаны из расчета за один комплект оборудования</div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <button class="btn-primary btn-connect">Купить оборудование</button>
                </div>
            </div>
        </div>
    </div>

    <div class="cooperation-options device-storage" style="padding-bottom: 50px">
        <div class="container">
            <div class="row">
                <div class="col wow fadeInLeft">
                    <div class="text1">Ответственное хранение</div>
                    <div class="text2"><span>+</span>доступ к платформе управления данными <br>
                        (без возможности самостоятельной выгрузки <br>на рекламные площадки)</div>
                </div>
            </div>

            <div class="row">
                <div class="col wow fadeInLeft">
                    <div class="text3">Комплект оборудования + доставка</div>
                    <div class="price1">бесплатно</div>
                    <p class="text6">Включает в себя аренду оборудования, сотовую связь, сбор, хранение, <br>
                        обработку данных и пользование личным кабинетом, без возможности <br>
                        прямой выгрузки данных на рекламные площадки MyTarget и Яндекс</p>
                    <div class="text5">Мы делимся с Вами</div>
                    <div class="price2">10%</div>
                    <p class="text6">прибыли от РК, проведённых на основе собранных данных <br>с ваших конструкций</p>
                </div>
            </div>

            <div class="row wow fadeInDown">
                <div class="col">
                    <button class="btn-primary btn-connect">Заказать оборудование</button>
                </div>
            </div>
        </div>
    </div>
</div>