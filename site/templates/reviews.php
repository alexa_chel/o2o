<?php namespace ProcessWire; ?>

<div class="page page-reviews">
    <a id="scroll-top">
        <img src="<?= $config->urls->templates ?>img/arrow-scroll-top.png" alt="">
    </a>

    <div class="container section-first">
        <div class="reviews-container">
            <div class="reviews">
                <div class="review" style="background: #ffffff url(<?= $config->urls->templates ?>img/preview-review-1.png) center / cover no-repeat">
                    <a class="review-hover iframe" href="https://www.youtube.com/embed/Xu0CXpJHRVo?autoplay=1&mute=1&enablejsapi=1">
                        <p class="review-hover-text1">Отзыв интернет-провайдера<br>«Вместе»</p>
                        <button class="btn-transparent">Смотреть</button>
                        <p class="review-hover-text2">г. Тюмень, 2019</p>
                    </a>
                </div>
                <div class="review" style="background: #ffffff url(<?= $config->urls->templates ?>img/preview-review-2.png) center / cover no-repeat">
                    <a class="review-hover iframe" href="https://www.youtube.com/embed/mNyGpMvTZ1s?autoplay=1&mute=1&enablejsapi=1">
                        <p class="review-hover-text1">Отзыв рекламной компании<br>«Деловой мир»</p>
                        <button class="btn-transparent">Смотреть</button>
                        <p class="review-hover-text2">г. Тюмень, 2019</p>
                    </a>
                </div>
                <div class="review" style="background: #ffffff url(<?= $config->urls->templates ?>img/preview-review-5.png) center / cover no-repeat">
                    <a class="review-hover iframe" href="https://www.youtube.com/embed/-dCsJea8Neg?autoplay=1&mute=1&enablejsapi=1">
                        <p class="review-hover-text1">Отзыв базы отдыха<br>«Куба»</p>
                        <button class="btn-transparent">Смотреть</button>
                        <p class="review-hover-text2">г. Тюмень, 2019</p>
                    </a>
                </div>
                <div class="review" style="background: #ffffff url(<?= $config->urls->templates ?>img/preview-review-6.png) center / cover no-repeat">
                    <a class="review-hover iframe" href="https://www.youtube.com/embed/6ZGZI3nCFP8?autoplay=1&mute=1&enablejsapi=1">
                        <p class="review-hover-text1">Отзыв агентства путешествий<br>«Vip Тур»</p>
                        <button class="btn-transparent">Смотреть</button>
                        <p class="review-hover-text2">г. Тюмень, 2019</p>
                    </a>
                </div>
                <div class="review" style="background: #ffffff url(http://i3.ytimg.com/vi/-K_fcwmPPjk/maxresdefault.jpg) center / cover no-repeat">
                    <a class="review-hover iframe" href="https://www.youtube.com/embed/-K_fcwmPPjk?autoplay=1&mute=1&enablejsapi=1">
                        <p class="review-hover-text1">Отзыв корпорации<br>«Бизнес-Мастер»</p>
                        <button class="btn-transparent">Смотреть</button>
                        <p class="review-hover-text2">г. Тюмень, 2020</p>
                    </a>
                </div>
                <div class="review review-small" style="background: #ffffff url(<?= $config->urls->templates ?>img/preview-review-3.png) center / contain no-repeat">
                    <div class="review-hover">
                        <p class="review-hover-text1">Отзыв автодилера<br>«Skoda»</p>
                        <button class="btn-transparent btn-read">Читать</button>
                        <p class="review-hover-text2">г. Тюмень, 2019</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-wrapper modal-wrapper-review">
        <div class="dialog-review modal">
            <div class="row review-company">
                <div class="company-photo"></div>
                <div class="company-about">
                    <div class="company-about__fio">Ирина Григорьева</div>
                    <div class="company-about__position">
                        Маркетолог автосалона SKODA,<br>
                        ООО «Дина Плюс»
                    </div>
                </div>
            </div>
            <div class="row separator"></div>
            <div class="row review-text">
                Компания О2О медиа, организовала для нашей компании<br>рекламу в интернете. Благодаря этому мы получили<br>новые целевые заявки, повысили трафик. Благодарим<br>за оперативное и слаженное сотрудничество!
            </div>
        </div>
    </div>
</div>