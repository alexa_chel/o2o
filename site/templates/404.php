<?php namespace ProcessWire; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Страница не найдена</title>
    <meta name="description" content="O2O media — это уникальное решение на основе WiFi технологии, позволяющее рекламным компаниям подтвердить маркетинговые показатели об аудитории outdoor рекламных конструкций и ретаргетировать их в online">
    <link rel="shortcut icon" href="<?= $config->urls->templates ?>img/o2o_favicon.png" type="image/png">
    <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="<?= $config->urls->templates ?>scripts/parallax.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>styles/fonts.css">
</head>
<style>
    html {
        width: 100%;
    }

    body {
        display: flex;
        justify-content: center;
        width: 100%;
        height: 100%;
        background-color: #f5f5f9;
        margin: 0;
    }

    main {
        width: 100%;
        height: 100vh;
        max-height: 850px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
    }

    #scene_404 {
        display: flex;
        justify-content: center;
        width: 685px;
    }

    #scene_404 .bang {
        width: 580px;
        top: 20px !important;
    }

    #scene_404 .number {
        position: absolute;
        left: 0;
        right: 0;
        margin: auto;
    }

    .page-404 .text {
        margin-top: 80px;
        margin-left: 140px;
    }

    .page-404 .text__caption {
        font-family: "PT Mono Bold", sans-serif;
        font-size: 28px;
        color: #054673;
        margin-bottom: 8px;
    }

    .page-404 .text__caption span {
        font-family: "TT Commons Medium", sans-serif;
    }

    .page-404 .text__description {
        font-family: "TT Commons", sans-serif;
        font-size: 18px;
        color: #9e9e9e;
        line-height: 1.2;
    }

    .page-404 .text__description a {
        color: #008beb;
        text-decoration: underline;
        text-underline-position: under;
        cursor: pointer;
    }

    .page-404 .text__description a:hover {
        color: #054673
    }

    @media (max-width: 767px) {
        body {
            padding: 0 20px;
            box-sizing: border-box;
        }

        #scene_404 {
            width: initial;
            align-items: center;
        }

        #scene_404 .number {
            width: 100%;
            max-width: 685px;
        }

        #scene_404 .bang {
            width: 85%;
            max-width: 580px;
        }

        .page-404 .text {
            margin-left: 10%;
        }
    }

    @media (max-width: 470px) {
        .page-404 .text__caption {
            font-size: 22px;
        }

        .page-404 .text__description {
            font-size: 15px;
        }
    }

    @media (max-width: 410px) {
        #scene_404 .bang {
            width: 83%;
            top: 10px !important;
        }

        .page-404 .text {
            margin-left: 0;
            width: 90%;
            text-align: center;
            margin-top: 40px;
        }

        .page-404 .text__description {
            font-size: 14px;
        }
    }

    @media (max-width: 350px) {
        #scene_404 .bang {
            width: 81%;
            top: 10px !important;
        }
    }
</style>
<body>
<main class="page-404">
    <div id="scene_404">
        <img class="bang" data-depth="0.2" src="<?= $config->urls->templates ?>img/bang.png" alt="">
        <img class="number" src="<?= $config->urls->templates ?>img/404-2.png" alt="">
    </div>
    <div class="text">
        <div class="text__caption">Страница не найдена <span>:(</span></div>
        <div class="text__description">
            Извините, но такой страницы на нашем сайте нет.<br>
            Возможно, введен неправильный адрес или страница была удалена.<br>
            Попробуйте вернуться <a href="/">на главную</a>
        </div>
    </div>
</main>
</body>
<script>
    if (document.getElementById('scene_404'))
        new Parallax(document.getElementById('scene_404'));
</script>
</html>
