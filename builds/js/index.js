$(function () {
    new Parallax(document.getElementById('scene'));
    new WOW().init();

    $('.strikethrough-wrapper1 .strikethrough').viewportChecker({
        classToAdd: 'go',
        offset: 100
    });

    $('.strikethrough-wrapper2 .strikethrough').viewportChecker({
        classToAdd: 'go',
        offset: 100
    });

    $('.strikethrough-wrapper3 .strikethrough').viewportChecker({
        classToAdd: 'go',
        offset: 100
    });

    $('.strikethrough-wrapper4 .strikethrough').viewportChecker({
        classToAdd: 'go',
        offset: 100
    });

    $('#bar').viewportChecker({
        classToAdd: 'go',
        offset: 100
    });

    $('.owl-carousel').owlCarousel({
        items: 10,
        nav: true,
        navText: ["<img src='img/aцrrow.svg' alt=''>","<img src='img/arrow.svg' alt=''>"]
    });

    disabledMonths();
    function disabledMonths() {
        const d = new Date();
        const month = d.getMonth()+1;

        for (let i = month + 1; i <= 12; i++) {
            $(`#month_${i}`).closest('.owl-item').addClass('disabled')
        }

        $(`#month_${month}`).append('<div class="strikethrough go"></div>')
    }

    let showMonthMacs = true;
    let oldMonthMacs;
    let start_date = moment().startOf('month').unix(), end_date = moment().unix();
    let myGroup;
    let map;
    ymaps.ready(init);
    function init() {
        map = new ymaps.Map("map", {
            center: [57.1568539, 65.5389583],
            zoom: 13
        });

        map.behaviors.disable('scrollZoom');

        $.get("https://sputniks.appspot.com/radars/get", data => {
            data = JSON.parse(data);

            if (data.status === "ok") {
                const radars = data.response;
                const content = ymaps.templateLayoutFactory.createClass(
                    '<div class="placemark-hint">$[properties.iconContent]</div>'
                );

                myGroup = new ymaps.GeoObjectCollection({}, {});

                Object.keys(radars).map(id => {
                    const placemark = new ymaps.Placemark(radars[id].split(', '), {
                        iconContent: '0',
                        id: id
                    }, {
                        iconLayout: 'default#imageWithContent',
                        iconImageHref: '../img/bord.png',
                        iconImageSize: [20, 21],
                        iconImageOffset: [-10, -10],
                        iconContentOffset: [13, -35],
                        iconContentLayout: content
                    });

                    myGroup.add(placemark);
                });

                map.geoObjects.add(myGroup);
                //map.setBounds(myGroup.getBounds());

                getMapData()
            }
        }).fail(() => console.error("error"));
    }

    $('#months .owl-item').click(function () {
        $('.strikethrough').remove();

        const $month = $(this).find('div');
        $month.append('<div class="strikethrough go"></div>');

        const d = new Date();
        const month = d.getMonth()+1;

        if (Number($month.attr('data-number')) === month) {
            start_date = moment().startOf('month').unix();
            end_date = moment().unix();
        } else {
            start_date = moment().add(-(month - Number($month.attr('data-number'))), 'months').startOf('month').unix();
            end_date = moment().add(-(month - Number($month.attr('data-number'))), 'months').endOf('month').unix()
        }

        getMapData()
    });

    function getMapData() {
        $.get(`https://sputniks.appspot.com/macs/counts-uniq?start=${start_date}&end=${end_date}`, data => {
            data = JSON.parse(data);

            if (data.status === "ok") {
                const data_obj = {};

                data.response.map(radar => {
                    data_obj[radar.radar_id] = radar.cnt
                });

                myGroup.each(function (el) {
                    const id = el.properties.get('id');

                    el.properties.set('iconContent', data_obj[id] ? numberWithSpaces(data_obj[id]) : 0);
                });

                $('#count-month').text(data_obj[-1]);

                if (showMonthMacs)
                    countup('count-month');
                else {
                    $('#count-month').attr('data-from', oldMonthMacs || 1);
                    $('#count-month').attr('data-to', Number(data_obj[-1]));
                    initSpincrement($('#count-month'));
                }

                showMonthMacs = false;
                oldMonthMacs = data_obj[-1];
            }
        }).fail(() => console.error("error"));
    }

    setInterval(getMapData, 60000);

    let showAllMacs = true;
    let oldAllMacs = null;
    getAllMacs();
    function getAllMacs() {
        $.get("https://sputniks.appspot.com/macs/count-all", data => {
            data = JSON.parse(data);

            if (data.status === "ok") {
                const count = data.response;

                $('#unique-macs').text(count);

                if (showAllMacs)
                    countup('unique-macs');
                else {
                    $('#unique-macs').attr('data-from', oldAllMacs || 1);
                    $('#unique-macs').attr('data-to', Number(count));
                    initSpincrement($('#unique-macs'));
                }

                showAllMacs = false;
                oldAllMacs = data.response;
            }
        }).fail(() => console.error("error"));


    }

    setInterval(getAllMacs, 60000);

    function countup(className){
        const countBlockTop = $("#"+className).offset().top;
        const windowHeight = window.innerHeight;
        let show = true;

        $(window).scroll( function (){
            if(show && (countBlockTop < $(window).scrollTop() + windowHeight)){
                show = false;

                $("#" + className).spincrement({
                    duration: 3000,
                    thousandSeparator: ' ',
                });
            }
        })
    }

    function initSpincrement(selector) {
        selector.spincrement({
            duration: 5000,
            thousandSeparator: ' ',
            fade: false
        });
    }

    /*function randomInteger(min, max) {
        let rand = min - 0.5 + Math.random() * (max - min + 1);
        return Math.round(rand);
    }*/

    function numberWithSpaces(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }
});